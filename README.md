ASP.NET Core back-end with a MySQL database to process logs from Logs.tf into a MySQL database. Vue front-end using the Bulma CSS framework to display collections of logs.

Requirements:
MySQL server (can be customized to other db type using different driver for the EntityFramework)
.NET Core 2.2

The application has an API endpoint that needs to be fed the IDs of logs. Once completed, it will return the combined data of all the players.
Mainly made for use with Highlander logs as it doesn't take into account very well that people might be running different classes.

To use, make sure you add a Steam API key in the appsettings.json file.
To set the connection string to your database server, add the connection string to your appsettings.json file.

The API is auto documented through the use of Swagger.