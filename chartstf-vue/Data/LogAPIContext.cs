using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace chartstf
{
    public class LogAPIContext : IdentityDbContext<SteamUser>
    {
        public LogAPIContext (DbContextOptions<LogAPIContext> options)
            : base(options)
        {
        }

        public DbSet<Statline> Statlines { get; set; }
        public DbSet<Match> Matches { get; set; }
        public DbSet<Player> Players { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<Collection> Collections { get; set; }
        public DbSet<CollectionsMatches> CollectionsMatches { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Statline>().ToTable("Statlines");
            modelBuilder.Entity<Match>().ToTable("Matches");
            modelBuilder.Entity<Player>().ToTable("Players");
            modelBuilder.Entity<Team>().ToTable("Teams");
            modelBuilder.Entity<Collection>().ToTable("Collections");
            modelBuilder.Entity<CollectionsMatches>().ToTable("CollectionsMatches");

            modelBuilder.Entity<Statline>(entity =>
            {
                entity.HasKey(s => s.ID);
                entity.Property(s => s.Steam3ID)
                    .IsRequired();
                entity.Property(s => s.MatchID)
                    .IsRequired();
                entity.HasOne(m => m.Match)
                      .WithMany(s => s.Statlines);
            });

            modelBuilder.Entity<Match>(entity =>
            {
                entity.HasKey(m => m.ID);
                entity.Property(m => m.Name)
                    .IsRequired()
                    .HasMaxLength(100);
                entity.Property(m => m.LogID)
                    .IsRequired()
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<Player>(entity =>
            {
                entity.HasKey(e => e.ID);
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(70);
            });

            modelBuilder.Entity<Team>(entity =>
            {
                entity.HasKey(e => e.ID);
                entity.Property(e => e.ETF2LID)
                    .IsRequired()
                    .HasMaxLength(30);
            });

            modelBuilder.Entity<Collection>(entity =>
            {
                entity.HasKey(e => e.ID);
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);
                entity.HasOne(m => m.SteamUser)
                      .WithMany(s => s.Collections);
                //entity.Property(e => e.IsPrivate)
                //    .HasDefaultValue(false);
            });

            modelBuilder.Entity<CollectionsMatches>().HasKey(pa => new { pa.MatchID, pa.CollectionID });

            //modelBuilder.Seed();
        }
    }
}
