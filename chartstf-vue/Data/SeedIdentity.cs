using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace chartstf
{
    public class SeedIdentity
    {
        private readonly UserManager<SteamUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;

        public SeedIdentity(UserManager<SteamUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            this._userManager = userManager;
            this._roleManager = roleManager;
        }

        public async Task SeedIdentityLogsAPI()
        {
            if (!(await _roleManager.RoleExistsAsync("Admin")))
            {
                var role = new IdentityRole("Admin");
                await _roleManager.CreateAsync(role);
            }

            if (!(await _roleManager.RoleExistsAsync("Player")))
            {
                var role = new IdentityRole("Player");
                await _roleManager.CreateAsync(role);
            }
        }
    }
}
