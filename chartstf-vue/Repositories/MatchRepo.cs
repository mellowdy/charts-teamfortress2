using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace chartstf
{
    public class MatchRepo : GenericRepo<Match>, IMatchRepo
    {
        private readonly LogAPIContext context;

        public MatchRepo(LogAPIContext context) : base(context)
        {
            this.context = context;
        }

        public async Task<Match> AddMatchAsync(Match match)
        {
            if (await MatchExistsAsync(match.LogID))
            {
                return match;
            }

            if (match.CollectionsMatches != null)
            {
                foreach (var cm in match.CollectionsMatches)
                {
                    cm.CollectionID = _context.Collections.Where<Collection>(c => c.Name == cm.Collection.Name).First().ID;
                    _context.Entry<Collection>(cm.Collection).State = EntityState.Detached;
                    _context.Entry<CollectionsMatches>(cm).State = EntityState.Added;
                }
            }

            _context.Matches.Add(match);
            await _context.SaveChangesAsync();
            return match;
        }

        public async Task<bool> MatchExistsAsync(string logID)
        {
            return await _context.Matches.AnyAsync(a => a.LogID == logID);
        }

        public async Task<Match> DeleteMatchAsync(Match match)
        {
            try
            {
                _context.Remove(match);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return match;
        }

        public async Task<Match> FindByIdAsync(Guid id)
        {
            var match = await this.context.Matches
                .Include(cm => cm.CollectionsMatches)
                    .ThenInclude(c => c.Collection)
                .Where(m => m.ID == id)
                .SingleOrDefaultAsync<Match>();
            return match;
        }

        public async Task<Match> FindByLogIdAsync(string logId)
        {
            var match = await this.context.Matches
                .Include(cm => cm.CollectionsMatches)
                    .ThenInclude(c => c.Collection)
                .Where(c => c.LogID == logId)
                .SingleOrDefaultAsync<Match>();
            return match;
        }

        public async Task<Collection> FindCollectionAsyncByName(string collectionName)
        {
            var collection = await this.context.Collections
                .Include(cm => cm.CollectionsMatches)
                .Where(c => c.Name == collectionName)
                .AsNoTracking()
                .FirstOrDefaultAsync<Collection>();
            return collection;
        }

        public async Task<Collection> FindCollectionAsyncById(Guid id)
        {
            var collection = await this.context.Collections
                .Include(c => c.CollectionsMatches)
                .Where(c => c.ID == id)
                .AsNoTracking()
                .FirstOrDefaultAsync<Collection>();
            return collection;
        }

        public new async Task<IEnumerable<Match>> GetAllAsync()
        {
            var matches = await this.context.Matches
                .Include(m => m.CollectionsMatches)
                    .ThenInclude(cm => cm.Collection)
                .OrderByDescending(m => m.Date)
                .AsNoTracking()
                .ToListAsync();
            return matches;
        }
    }
}
