using System;
using System.Collections.Generic;
using System.Text;

namespace chartstf
{
    public class GroupedClassStatlineMatchModel
    {
        public List<GroupedClassStatline> GroupedClassStatlines { get; set; }
        public List<Match> Matches { get; set; }
    }
}
