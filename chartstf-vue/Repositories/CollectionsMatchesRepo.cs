using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace chartstf
{
    public class CollectionsMatchesRepo : GenericRepo<CollectionsMatches>, ICollectionsMatchesRepo
    {
        private readonly LogAPIContext context;

        public CollectionsMatchesRepo(LogAPIContext context) : base(context)
        {
            this.context = context;
        }

        public async Task<IEnumerable<CollectionsMatches>> AddCollectionsMatchesAsync(IEnumerable<CollectionsMatches> collectionsMatches)
        {
            await _context.AddRangeAsync(collectionsMatches);
            await _context.SaveChangesAsync();
            return collectionsMatches;
        }

        public async Task<CollectionsMatches> CollectionsMatchesExistsAsync(Guid matchId, Guid collectionId)
        {
            return await _context.CollectionsMatches.Where(wc => (wc.MatchID == matchId) && (wc.CollectionID == collectionId)).FirstOrDefaultAsync();
        }
    }
}
