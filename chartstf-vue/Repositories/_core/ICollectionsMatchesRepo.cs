using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace chartstf
{
    public interface ICollectionsMatchesRepo : IGenericRepo<CollectionsMatches>
    {
        Task<IEnumerable<CollectionsMatches>> AddCollectionsMatchesAsync(IEnumerable<CollectionsMatches> collectionsMatches);
    }
}
