using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace chartstf
{
    public interface IStatlineRepo : IGenericRepo<Statline>
    {
        Task<IEnumerable<Statline>> AddStatlinesAsync(IEnumerable<Statline> statlines);
        Task<bool> StatlineExistsAsync(Guid matchID);
        Task<IEnumerable<Statline>> GetStatlinesFromLog(string logID);
        IEnumerable<GroupedClassStatline> GetGroupedStatlines(IEnumerable<Statline> generalStatlines);
        Task<IEnumerable<GroupedClassStatline>> GetGroupedStatlinesFromCollection(Guid collectionID);
        Task<IEnumerable<Statline>> GetStatlinesFromMatch(Match match);
    }
}
