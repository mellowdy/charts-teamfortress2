using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace chartstf
{
    public interface ICollectionRepo : IGenericRepo<Collection>
    {
        Task<bool> CollectionExistsAsync(string name);
        Task<Collection> FindByIdAsync(Guid collectionId);
        new Task<IEnumerable<Collection>> GetAllAsync();
        Task<Collection> AddCollectionAsync(Collection collection);
        Task<Collection> FindByShortUrlAsync(string shortUrl);
        Task<Collection> GenerateShortUrl(Collection collection);
        Task<Collection> DeleteCollectionAsync(Collection collection);
        Task<IEnumerable<Collection>> GetCollectionsBySteamIdAsync(ulong steamId);
        Task<IEnumerable<Collection>> GetCollectionsByUserIdAsync(string userId);
        Task<IEnumerable<Collection>> GetPublicAsync();
    }
}
