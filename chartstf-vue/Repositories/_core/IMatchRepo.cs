using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace chartstf
{
    public interface IMatchRepo : IGenericRepo<Match>
    {
        new Task<IEnumerable<Match>> GetAllAsync();
        Task<Match> AddMatchAsync(Match match);
        Task<bool> MatchExistsAsync(string logID);
        Task<Match> DeleteMatchAsync(Match match);
        Task<Match> FindByIdAsync(Guid id);
        Task<Match> FindByLogIdAsync(string logId);
        Task<Collection> FindCollectionAsyncByName(string collectionName);
        Task<Collection> FindCollectionAsyncById(Guid id);
    }
}
