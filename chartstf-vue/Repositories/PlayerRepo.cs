using System;
using System.Collections.Generic;
using System.Text;

namespace chartstf
{
    public class PlayerRepo : GenericRepo<Player>, IPlayerRepo
    {
        private readonly LogAPIContext context;

        public PlayerRepo(LogAPIContext context) : base(context)
        {
            this.context = context;
        }
    }
}
