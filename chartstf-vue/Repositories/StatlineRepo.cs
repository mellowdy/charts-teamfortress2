using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace chartstf
{
    public class StatlineRepo : GenericRepo<Statline>, IStatlineRepo
    {
        private readonly LogAPIContext context;

        public StatlineRepo(LogAPIContext context) : base(context)
        {
            this.context = context;
        }

        public async Task<IEnumerable<Statline>> AddStatlinesAsync(IEnumerable<Statline> statlines)
        {
            await _context.AddRangeAsync(statlines);
            await _context.SaveChangesAsync();
            return statlines;
        }

        public async Task<bool> StatlineExistsAsync(Guid matchID)
        {
            return await _context.Statlines.AnyAsync(a => a.MatchID == matchID);
        }

        public async Task<IEnumerable<Statline>> GetStatlinesFromLog(string logID)
        {
            return await _context.Statlines
                .Where(s => s.LogID == logID)
                .AsNoTracking()
                .ToListAsync();
        }

        public IEnumerable<GroupedClassStatline> GetGroupedStatlines(IEnumerable<Statline> generalStatlines)
        {
            List<GroupedClassStatline> groupedClassStatlines = new List<GroupedClassStatline>();
            var requestedPlayerGroupedStatlines = generalStatlines.GroupBy(s => s.Alias);

            foreach (IGrouping<string, Statline> statlinePlayerGroupDictionary in requestedPlayerGroupedStatlines)
            {
                List<Statline> playerStatlines = new List<Statline>();
                playerStatlines = statlinePlayerGroupDictionary.ToList<Statline>();
                var requestedClassGroupedStatlines = playerStatlines.GroupBy(s => s.Class);
                //Debug.WriteLine("Processing stats for " + statlinePlayerGroupDictionary.Key);
                foreach (IGrouping<string, Statline> statlineClassGroupDictionary in requestedClassGroupedStatlines)
                {
                    //Debug.WriteLine("Processing " + statlineClassGroupDictionary.Key + " stats for " + statlinePlayerGroupDictionary.Key);
                    GroupedClassStatline groupedClassStatline = new GroupedClassStatline();
                    groupedClassStatline.Class = statlineClassGroupDictionary.Key;
                    List<Statline> statlines = new List<Statline>();
                    statlines = statlineClassGroupDictionary.ToList<Statline>();
                    groupedClassStatline.Alias = statlines.Select(s => s.Alias).FirstOrDefault();
                    groupedClassStatline.Time = statlines.Aggregate(TimeSpan.Zero, (currentSum, sl) => currentSum + sl.Time);
                    groupedClassStatline.Kills = statlines.Sum(s => s.Kills);
                    groupedClassStatline.Deaths = statlines.Sum(s => s.Deaths);
                    groupedClassStatline.Assists = statlines.Sum(s => s.Assists);
                    groupedClassStatline.Damage = statlines.Sum(s => s.Damage);
                    groupedClassStatline.Suicides = statlines.Sum(s => s.Suicides);
                    groupedClassStatline.Caps = statlines.Sum(s => s.Caps);
                    groupedClassStatline.HealthPacks = statlines.Sum(s => s.HealthPacks);
                    groupedClassStatline.HealsReceived = statlines.Sum(s => s.HealsReceived);
                    groupedClassStatline.HealPercentageReceived = statlines.Average(s => s.HealPercentageReceived);
                    groupedClassStatline.Airshots = statlines.Sum(s => s.Airshots);
                    groupedClassStatline.DamageTaken = statlines.Sum(s => s.DamageTaken);

                    // Stats based on time
                    groupedClassStatline.DamagePerMinute = (double)groupedClassStatline.Damage / groupedClassStatline.Time.TotalMinutes;
                    groupedClassStatline.KillsAssistsPerMinute = ((double)groupedClassStatline.Kills + (double)groupedClassStatline.Assists) / groupedClassStatline.Time.TotalMinutes;
                    groupedClassStatline.KillsPerMinute = (double)groupedClassStatline.Kills / groupedClassStatline.Time.TotalMinutes;
                    groupedClassStatline.DeathsPerMinute = (double)groupedClassStatline.Deaths / groupedClassStatline.Time.TotalMinutes;
                    groupedClassStatline.AssistsPerMinute = (double)groupedClassStatline.Assists / groupedClassStatline.Time.TotalMinutes;
                    groupedClassStatline.AirshotsPerMinute = (double)groupedClassStatline.Airshots / groupedClassStatline.Time.TotalMinutes;
                    groupedClassStatline.DamageTakenPerMinute = (double)groupedClassStatline.DamageTaken / groupedClassStatline.Time.TotalMinutes;

                    // Buildings
                    groupedClassStatline.SentriesBuilt = statlines.Sum(s => s.SentriesBuilt);
                    groupedClassStatline.DispensersBuilt = statlines.Sum(s => s.DispensersBuilt);
                    groupedClassStatline.TeleportersBuilt = statlines.Sum(s => s.TeleportersBuilt);

                    groupedClassStatline.SentriesDestroyed = statlines.Sum(s => s.SentriesDestroyed);
                    groupedClassStatline.DispensersDestroyed = statlines.Sum(s => s.DispensersDestroyed);
                    groupedClassStatline.TeleportersDestroyed = statlines.Sum(s => s.TeleportersDestroyed);

                    groupedClassStatline.SentriesBuiltPerMinute = (double)groupedClassStatline.SentriesBuilt / groupedClassStatline.Time.TotalMinutes;
                    groupedClassStatline.SentriesDestroyedPerMinute = (double)groupedClassStatline.SentriesDestroyed / groupedClassStatline.Time.TotalMinutes;

                    groupedClassStatline.DispensersBuiltPerMinute = (double)groupedClassStatline.DispensersBuilt / groupedClassStatline.Time.TotalMinutes;
                    groupedClassStatline.DispensersDestroyedPerMinute = (double)groupedClassStatline.DispensersDestroyed / groupedClassStatline.Time.TotalMinutes;

                    groupedClassStatline.TeleportersBuiltPerMinute = (double)groupedClassStatline.TeleportersBuilt / groupedClassStatline.Time.TotalMinutes;
                    groupedClassStatline.TeleportersDestroyedPerMinute = (double)groupedClassStatline.TeleportersDestroyed / groupedClassStatline.Time.TotalMinutes;

                    groupedClassStatline.Extinguishes = statlines.Sum(s => s.Extinguishes);
                    groupedClassStatline.ExtinguishesPerMinute = (double)groupedClassStatline.Extinguishes / groupedClassStatline.Time.TotalMinutes;

                    // Stats based on deaths
                    if (groupedClassStatline.Deaths != 0)
                    {
                        groupedClassStatline.KillsPerDeaths = (double)groupedClassStatline.Kills / (double)groupedClassStatline.Deaths;
                        groupedClassStatline.KillsAssistsPerDeaths = ((double)groupedClassStatline.Kills + (double)groupedClassStatline.Assists) / (double)groupedClassStatline.Deaths;
                    }
                    
                    groupedClassStatline.ScoutKills = statlines.Sum(s => s.ScoutKills);
                    groupedClassStatline.ScoutKillsPerMinute = (double)groupedClassStatline.ScoutKills / groupedClassStatline.Time.TotalMinutes;
                    groupedClassStatline.SoldierKills = statlines.Sum(s => s.SoldierKills);
                    groupedClassStatline.SoldierKillsPerMinute = (double)groupedClassStatline.SoldierKills / groupedClassStatline.Time.TotalMinutes;
                    groupedClassStatline.PyroKills = statlines.Sum(s => s.PyroKills);
                    groupedClassStatline.PyroKillsPerMinute = (double)groupedClassStatline.PyroKills / groupedClassStatline.Time.TotalMinutes;
                    groupedClassStatline.DemomanKills = statlines.Sum(s => s.DemomanKills);
                    groupedClassStatline.DemomanKillsPerMinute = (double)groupedClassStatline.DemomanKills / groupedClassStatline.Time.TotalMinutes;
                    groupedClassStatline.HeavyKills = statlines.Sum(s => s.HeavyKills);
                    groupedClassStatline.HeavyKillsPerMinute = (double)groupedClassStatline.HeavyKills / groupedClassStatline.Time.TotalMinutes;
                    groupedClassStatline.EngineerKills = statlines.Sum(s => s.EngineerKills);
                    groupedClassStatline.EngineerKillsPerMinute = (double)groupedClassStatline.EngineerKills / groupedClassStatline.Time.TotalMinutes;
                    groupedClassStatline.MedicKills = statlines.Sum(s => s.MedicKills);
                    groupedClassStatline.MedicKillsPerMinute = (double)groupedClassStatline.MedicKills / groupedClassStatline.Time.TotalMinutes;
                    groupedClassStatline.SniperKills = statlines.Sum(s => s.SniperKills);
                    groupedClassStatline.SniperKillsPerMinute = (double)groupedClassStatline.SniperKills / groupedClassStatline.Time.TotalMinutes;
                    groupedClassStatline.SpyKills = statlines.Sum(s => s.SpyKills);
                    groupedClassStatline.SpyKillsPerMinute = (double)groupedClassStatline.SpyKills / groupedClassStatline.Time.TotalMinutes;
                    groupedClassStatline.ScoutDeaths = statlines.Sum(s => s.ScoutDeaths);
                    groupedClassStatline.SoldierDeaths = statlines.Sum(s => s.SoldierDeaths);
                    groupedClassStatline.PyroDeaths = statlines.Sum(s => s.PyroDeaths);
                    groupedClassStatline.DemomanDeaths = statlines.Sum(s => s.DemomanDeaths);
                    groupedClassStatline.HeavyDeaths = statlines.Sum(s => s.HeavyDeaths);
                    groupedClassStatline.EngineerDeaths = statlines.Sum(s => s.EngineerDeaths);
                    groupedClassStatline.MedicDeaths = statlines.Sum(s => s.MedicDeaths);
                    groupedClassStatline.MedicPicks = statlines.Sum(s => s.MedicPicks);
                    groupedClassStatline.MedicPicksPerMinute = (double)groupedClassStatline.MedicPicks / groupedClassStatline.Time.TotalMinutes;
                    groupedClassStatline.SniperDeaths = statlines.Sum(s => s.SniperDeaths);
                    groupedClassStatline.SpyDeaths = statlines.Sum(s => s.SpyDeaths);

                    if (groupedClassStatline.Class.Contains("spy"))
                    {
                        groupedClassStatline.Backstabs = statlines.Sum(s => s.Backstabs);
                        groupedClassStatline.BackstabsPerMinute = (double)groupedClassStatline.Backstabs / groupedClassStatline.Time.TotalMinutes;
                    }

                    if (groupedClassStatline.Class.Contains("spy") || groupedClassStatline.Class.Contains("sniper"))
                    {
                        groupedClassStatline.Headshots = statlines.Sum(s => s.Headshots);
                        groupedClassStatline.HeadshotsPerMinute = (double)groupedClassStatline.Headshots / groupedClassStatline.Time.TotalMinutes;
                    }

                    if (groupedClassStatline.Class.Contains("soldier") || groupedClassStatline.Class.Contains("medic"))
                    {
                        groupedClassStatline.Healing = statlines.Sum(s => s.Healing);
                        groupedClassStatline.HealingPerMinute = (double)groupedClassStatline.Healing / groupedClassStatline.Time.TotalMinutes;
                    }

                    if (groupedClassStatline.Class.Contains("medic"))
                    {
                        groupedClassStatline.AdvantagesLost = statlines.Sum(s => s.AdvantagesLost);
                        groupedClassStatline.AvgTimeBeforeHealing = statlines.Average(s => s.AvgTimeBeforeHealing);
                        groupedClassStatline.AvgBuildTime = statlines.Average(s => s.AvgBuildTime);
                        groupedClassStatline.AvgTimeBeforeUsing = statlines.Average(s => s.AvgTimeBeforeUsing);
                        groupedClassStatline.AvgUberLength = statlines.Average(s => s.AvgUberLength);
                        groupedClassStatline.BiggestAdvantageLost = statlines.Max(s => s.BiggestAdvantageLost);
                        groupedClassStatline.Ubers = statlines.Sum(s => s.Ubers);
                        groupedClassStatline.UbersDropped = statlines.Sum(s => s.UbersDropped);
                        groupedClassStatline.UbersPerMinute = (double)groupedClassStatline.Ubers / groupedClassStatline.Time.TotalMinutes;

                        // Uber types
                        groupedClassStatline.UberTypes = new List<Medigun>();
                        groupedClassStatline.UberTypes.Add(new Medigun() { Name = "Medigun", Ubers = statlines.Sum(s => s.Medigun) });
                        groupedClassStatline.UberTypes.Add(new Medigun() { Name = "Kritzkrieg", Ubers = statlines.Sum(s => s.Kritzkrieg) });
                        groupedClassStatline.UberTypes.Add(new Medigun() { Name = "Quick-Fix", Ubers = statlines.Sum(s => s.QuickFix) });
                        groupedClassStatline.UberTypes.Add(new Medigun() { Name = "Vaccinator", Ubers = statlines.Sum(s => s.Vaccinator) });

                        //groupedClassStatline.UberTypes = new object[][] ["Medigun", ""];
                        //groupedClassStatline.Medigun = statlines.Sum(s => s.Medigun);
                        //groupedClassStatline.Kritzkrieg = statlines.Sum(s => s.Kritzkrieg);
                        //groupedClassStatline.QuickFix = statlines.Sum(s => s.QuickFix);
                        //groupedClassStatline.Vaccinator = statlines.Sum(s => s.Vaccinator);

                        groupedClassStatline.DeathsAfterCharge = statlines.Sum(s => s.DeathsAfterCharge);
                        groupedClassStatline.DeathsCloseToUber = statlines.Sum(s => s.DeathsCloseToUber);
                    }

                    groupedClassStatlines.Add(groupedClassStatline);
                }
            }

            return groupedClassStatlines;
        }

        public async Task<IEnumerable<GroupedClassStatline>> GetGroupedStatlinesFromCollection(Guid collectionID)
        {
            Collection collection = await _context.Collections
                .Where(c => c.ID == collectionID)
                .Include(s => s.CollectionsMatches)
                .ThenInclude(s => s.Match)
                .ThenInclude(s => s.Statlines)
                .AsNoTracking()
                .SingleOrDefaultAsync();
            List<Statline> statlines = collection.CollectionsMatches.SelectMany(s => s.Match.Statlines).ToList();
            List<GroupedClassStatline> classStatlines = GetGroupedStatlines(statlines).ToList();
            return classStatlines;
        }

        public async Task<IEnumerable<Statline>> GetStatlinesFromMatch(Match match)
        {
            return await _context.Matches.Where(m => m.ID == match.ID)
                .Include(m => m.Statlines)
                .SelectMany(m => m.Statlines)
                .AsNoTracking()
                .ToListAsync();
        }
    }
}
