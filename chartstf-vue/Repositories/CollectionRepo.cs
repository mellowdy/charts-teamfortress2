using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace chartstf
{
    public class CollectionRepo : GenericRepo<Collection>, ICollectionRepo
    {
        private readonly LogAPIContext context;

        public CollectionRepo(LogAPIContext context) : base(context)
        {
            this.context = context;
        }

        public async Task<bool> CollectionExistsAsync(string name)
        {
            return await _context.Collections.AnyAsync(a => a.Name == name);
        }

        public async Task<Collection> FindByIdAsync(Guid collectionId)
        {
            var collection = await this._context.Collections
                .Include(c => c.CollectionsMatches)
                    .ThenInclude(cm => cm.Match)
                .Where(c => c.ID == collectionId)
                .FirstOrDefaultAsync<Collection>();
            return collection;
        }

        public async Task<Collection> FindByShortUrlAsync(string shortUrl)
        {
            var collection = await this._context.Collections
                .Include(c => c.CollectionsMatches)
                    .ThenInclude(cm => cm.Match)
                .Where(c => c.ShortUrl == shortUrl)
                .FirstOrDefaultAsync<Collection>();
            return collection;
        }

        public new async Task<IEnumerable<Collection>> GetAllAsync()
        {
            var collections = await this.context.Collections
                .Include(c => c.CollectionsMatches)
                    .ThenInclude(cm => cm.Match)
                .OrderBy(c => c.Name)
                .AsNoTracking()
                .ToListAsync();
            return collections;
        }

        public async Task<IEnumerable<Collection>> GetPublicAsync()
        {
            var collections = await this.context.Collections
                .Include(c => c.CollectionsMatches)
                    .ThenInclude(cm => cm.Match)
                .Where(c => c.IsPrivate == false)
                .OrderBy(c => c.Name)
                .AsNoTracking()
                .ToListAsync();
            return collections;
        }

        public async Task<IEnumerable<Collection>> GetCollectionsByUserIdAsync(string userId)
        {
            var collections = await this.context.Collections
                .Include(c => c.CollectionsMatches)
                    .ThenInclude(cm => cm.Match)
                .Where(c => c.UserId == userId)
                .OrderBy(c => c.Name)
                .AsNoTracking()
                .ToListAsync();
            return collections;
        }

        public async Task<IEnumerable<Collection>> GetCollectionsBySteamIdAsync(ulong steamId)
        {
            var collections = await this.context.Collections
                .Include(c => c.CollectionsMatches)
                    .ThenInclude(cm => cm.Match)
                .Where(c => c.SteamId == steamId)
                .OrderBy(c => c.Name)
                .AsNoTracking()
                .ToListAsync();
            return collections;
        }

        public async Task<Collection> AddCollectionAsync(Collection collection)
        {
            if (await CollectionExistsAsync(collection.Name))
            {
                return collection;
            }
            collection.CollectionsMatches = null;
            _context.Collections.Add(collection);
            await _context.SaveChangesAsync();
            return collection;
        }

        public async Task<Collection> DeleteCollectionAsync(Collection collection)
        {
            try
            {
                _context.Remove(collection);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return collection;
        }

        public async Task<Collection> GenerateShortUrl(Collection collectionWithUpdate)
        {
            try
            {
                collectionWithUpdate.ShortUrl = Convert.ToBase64String(collectionWithUpdate.ID.ToByteArray())
                    .Substring(0,10)
                    .Replace("/", "_")
                    .Replace("+", "-");
                _context.Entry(collectionWithUpdate).Property(w => w.ShortUrl).IsModified = true;
                _context.Update(collectionWithUpdate);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw ex;
            }

            return collectionWithUpdate;
        }
    }
}
