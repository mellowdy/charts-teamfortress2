using System;
using System.Collections.Generic;
using System.Text;

namespace chartstf
{
    public class TeamRepo : GenericRepo<Team>, ITeamRepo
    {
        private readonly LogAPIContext context;

        public TeamRepo(LogAPIContext context) : base(context)
        {
            this.context = context;
        }
    }
}
