using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace chartstf
{
    public interface ISteamService
    {
        Task<SteamPlayer> GetSteamPlayer(ulong steamId);
    }
}
