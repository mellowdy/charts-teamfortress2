using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace chartstf
{
    public interface ILogService
    {
        //Task<Match> ParseLog(string logID);
        Task<Match> ParseLog(string logID);
        Task<GroupedClassStatlineMatchModel> ParseLogs(List<string> logIDs, string userId = null, string connectionId = null);
    }
}
