using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace chartstf
{
    public class SteamService : ISteamService
    {
        private string apiKey;

        public SteamService(IConfiguration configuration)
        {
            this.apiKey = configuration["ApiKeys:Steam"];
        }

        public async Task<SteamPlayer> GetSteamPlayer(ulong steamId)
        {
            // Store information about player
            SteamPlayer steamPlayer = new SteamPlayer();

            using (HttpClient client = new HttpClient())
            {
                var response = await client.GetAsync("http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=" + apiKey + "&steamids=" + steamId);

                try
                {
                    response.EnsureSuccessStatusCode();

                    var stringResponse = await response.Content.ReadAsStringAsync();
                    dynamic json = JObject.Parse(stringResponse);
                    dynamic player = json.response.players[0];
                    steamPlayer.SteamId = Convert.ToUInt64(player.steamid.Value);
                    steamPlayer.PersonaName = player.personaname.Value;
                    steamPlayer.Avatar = player.avatar.Value;
                    steamPlayer.AvatarMedium = player.avatarmedium.Value;
                    steamPlayer.AvatarFull = player.avatarfull.Value;
                    steamPlayer.ProfileUrl = "https://steamcommunity.com/profiles/" + steamPlayer.SteamId;
                }
                catch (HttpRequestException ex)
                {
                    Debug.WriteLine($"User does not exist or something went wrong: {ex}");
                    return null;
                }
            }

            return steamPlayer;
        }
    }
}
