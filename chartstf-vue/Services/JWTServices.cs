using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace chartstf
{
    public class JWTServices<TEntity> where TEntity : SteamUser
    {
        private readonly ILogger logger;
        private readonly UserManager<SteamUser> userManager;
        private readonly IConfiguration configuration;

        public JWTServices(IConfiguration configuration, ILogger logger, UserManager<SteamUser> userManager)
        {
            this.logger = logger;
            this.userManager = userManager;
            this.configuration = configuration;
        }

        public async Task<object> GenerateJwtToken(string username)
        {
            try
            {
                SteamUser user = await userManager.FindByNameAsync(username);

                await userManager.AddClaimAsync(user, new Claim("myExtraKey", "myExtraValue"));
                var userClaims = await userManager.GetClaimsAsync(user);

                var claims = new List<Claim>
                {
                     new Claim(JwtRegisteredClaimNames.Sub, username),
                     // new Claim(ClaimTypes.NameIdentifier, identityModel.UserName),
                     new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                }.Union(userClaims);

                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["Tokens:Key"]));
                var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                var token = new JwtSecurityToken(
                    issuer: configuration["Tokens:Issuer"],
                    audience: configuration["Tokens:Audience"],
                    claims: claims,
                    expires: DateTime.UtcNow.AddDays(Convert.ToDouble(configuration["Tokens: Expires"])),
                    signingCredentials: creds
                );

                SteamUser player = await userManager.FindByIdAsync(user.Id);

                return new
                {
                    token = new JwtSecurityTokenHandler().WriteToken(token),
                    expiration = token.ValidTo,
                    user = new
                    {
                        userName = user.UserName,
                        personaName = user.PersonaName,
                        profileUrl = user.ProfileUrl,
                        avatarUrl = user.AvatarFull
                    }
                };
            }
            catch (Exception exc)
            {
                logger.LogError($"Exception thrown when creating JWT: {exc}");
            }
            return new { error = "Failed to generate JWT token." };
        }
    }
}
