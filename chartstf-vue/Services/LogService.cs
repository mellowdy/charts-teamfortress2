using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using System.Net;
using System.ComponentModel;
using System.IO;
using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.Core;

namespace chartstf
{
    public class LogService : ILogService
    {
        private IMatchRepo _matchRepo;
        private IStatlineRepo _statlineRepo;
        private ICollectionRepo _collectionRepo;

        public LogService(IMatchRepo matchRepo, IStatlineRepo statlineRepo, ICollectionRepo collectionRepo)
        {
            this._matchRepo = matchRepo;
            this._statlineRepo = statlineRepo;
            this._collectionRepo = collectionRepo;
        }

        public async Task<Match> ParseLog(string logID)
        {
            // Create new HTTP client
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("Accept", "application/json");

            // Add log id
            string url = "https://logs.tf/json/" + logID;
            string zipUrl = "http://logs.tf/logs/log_" + logID + ".log.zip";
            WebClient web = new WebClient();
            Stream data = web.OpenRead(new Uri(zipUrl));
            UnzipFromStream(data, @"logs");

            List<string> lines = System.IO.File.ReadAllLines(@"logs\log_" + logID + ".log").ToList();
            List<string> roundLines = lines.Where(s => s.Contains("World triggered \"Round_Length\"") || s.Contains("World triggered \"Mini_Round_Length\"")).ToList();

            double totalTime = 0.0;
            foreach (string line in roundLines)
            {
                string[] roundTime = line.Split("\"");
                double time = double.Parse(roundTime[3], System.Globalization.CultureInfo.InvariantCulture);
                totalTime += time;
                //Debug.WriteLine("Round time: " + roundTime[3] + ": " + time.ToString());
            }
            
            //web.DownloadFileCompleted += web_DownloadFileCompleted;
            //web.DownloadFileAsync(new Uri(zipUrl), @"J:\logs\" + logID + ".zip");

            // Fetch json log
            var response = await client.GetStringAsync(url);
            // Parse log into JObject
            dynamic log = JObject.Parse(response);
            // Make new match
            Match match = new Match();

            match.Map = log.info.map;
            match.Name = log.info.title;
            match.Date = UnixTimeStampToDateTime(Convert.ToDouble((log.info.date)));
            match.Time = TimeSpan.FromSeconds(Convert.ToDouble(log.length));
            // Actual playtime (pauses removed)
            if (totalTime != 0.0)
            {
                match.PlayTime = TimeSpan.FromSeconds(Math.Round(totalTime, 0));
            }
            else
            {
                // In some VERY rare situations, logs do not contain the proper time played. In this case, resort back to the regular time.
                match.PlayTime = TimeSpan.FromSeconds(Convert.ToDouble(log.length));
            }
            match.LogID = logID;

            //Debug.WriteLine("Time played: " + match.PlayTime);
            //Debug.WriteLine("Total time: " + match.Time);

            Match matchPresent = await _matchRepo.FindByLogIdAsync(logID);

            if (matchPresent != null)
            {
                //match = matchPresent;
                return matchPresent;
                //await _statlineRepo.GetStatlinesFromLog(logID);
            }

            // Collect statlines
            List<Statline> statlines = new List<Statline>();

            match = await _matchRepo.AddMatchAsync(match);

            // General player stats
            dynamic players = log.players;

            // Kills gotten on certain classes
            dynamic classKills = log.classkills;

            // Deaths to certain classes
            dynamic classDeaths = log.classdeaths;

            // Heals received
            dynamic healSpread = log.healspread;

            foreach (dynamic player in players)
            {
                Statline statline = new Statline();
                // Steam3ID
                statline.Steam3ID = player.Name;
                statline.MatchID = match.ID;
                statline.LogID = logID;
                // Fetch ETF2L info
                string profileETF2L = "http://api.etf2l.org/player/" + statline.Steam3ID;
                var responseETF2L = await client.GetStringAsync(profileETF2L);
                dynamic infoETF2L = JObject.Parse(responseETF2L);
                dynamic status = infoETF2L.status.code;
                // If successful, get user alias
                if (status == 200)
                {
                    statline.Alias = infoETF2L.player.name;
                }
                // Generic stats
                dynamic regularStats = player.Value.class_stats[0];
                if (Convert.ToDouble(regularStats.total_time) != 0.0)
                {
                    statline.Time = TimeSpan.FromSeconds(Convert.ToDouble(regularStats.total_time));
                }
                else
                {
                    try
                    {
                        statline.Time = match.PlayTime;
                        Debug.WriteLine("Warning: a statline was found for player " + statline.Alias + " with 0 time registered. Assigned overall match time as statline time.");
                    } catch (Exception ex)
                    {
                        statline.Time = TimeSpan.FromSeconds(1.0);
                        Debug.WriteLine("Warning: a statline was found for player " + statline.Alias + " with 0 time registered. Manually adjust time in the database." + ex);
                    } 
                }

                if (statline.Time > match.PlayTime)
                {
                    statline.Time = match.PlayTime;
                }

                statline.Class = regularStats.type;
                statline.Kills = regularStats.kills;
                statline.Assists = regularStats.assists;
                statline.Deaths = regularStats.deaths;
                statline.Damage = regularStats.dmg;
                statline.Caps = player.Value.cpc;
                statline.Suicides = player.Value.suicides;

                // Logs from around the 2013 era do not contain airshots hit.
                try
                {
                    statline.Airshots = player.Value.@as;
                } catch (Exception ex)
                {
                    Debug.WriteLine("Old log: no airshots processed for this player." + ex);
                }

                // Logs from around the 2013 era do not contain airshots hit.
                try
                {
                    statline.Airshots = player.Value.@as;
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Old log: no airshots processed for this player." + ex);
                }

                // Logs from around the 2013 era may or may not contain damage taken.
                try
                {
                    statline.DamageTaken = player.Value.dt;
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Old log: no airshots processed for this player." + ex);
                }

                statline.HealthPacks = player.Value.medkits;

                statline.KillsPerMinute = (double)statline.Kills / (double)statline.Time.TotalMinutes;
                statline.DeathsPerMinute = (double)statline.Deaths / statline.Time.TotalMinutes;
                statline.AssistsPerMinute = (double)statline.Assists / statline.Time.TotalMinutes;
                statline.KillsAssistsPerMinute = ((double)statline.Kills + (double)statline.Assists) / statline.Time.TotalMinutes;
                statline.DamagePerMinute = (double)statline.Damage / statline.Time.TotalMinutes;
                
                // Logs from the 2013 era do not mention airshots hit. 
                try
                {
                    statline.AirshotsPerMinute = (double)statline.Airshots / statline.Time.TotalMinutes;
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Cannot calculate airshots per minute: old log." + ex);
                }

                // Logs from the 2013 era may or may not contain damage taken. If not, ignore.
                try
                {
                    statline.DamageTakenPerMinute = (double)statline.DamageTaken / statline.Time.TotalMinutes;
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Cannot calculate damage taken per minute: old log." + ex);
                }               

                if (statline.Deaths != 0)
                {
                    statline.KillsPerDeaths = (double)statline.Kills / (double)statline.Deaths;
                    statline.KillsAssistsPerDeaths = ((double)statline.Kills + (double)statline.Assists) / (double)statline.Deaths;
                }

                if (statline.Class == "spy" || statline.Class == "sniper")
                {
                    // Old logs only count headshot kills and not individual headshots.
                    try
                    {
                        statline.Headshots = player.Value.headshots_hit;
                        statline.HeadshotsPerMinute = (double)statline.Headshots / statline.Time.TotalMinutes;
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine("Could not process headshots: old log. " + ex);
                    }
                }

                if (statline.Class == "spy")
                {
                    statline.Backstabs = player.Value.backstabs;
                    statline.BackstabsPerMinute = (double)statline.Backstabs / statline.Time.TotalMinutes;
                }

                if (statline.Class == "medic" || statline.Class == "soldier")
                {
                    statline.Healing = player.Value.heal;
                    statline.HealingPerMinute = (double)statline.Healing / statline.Time.TotalMinutes;
                }

                if (statline.Class == "medic")
                {
                    statline.Ubers = player.Value.ubers;
                    statline.UbersPerMinute = statline.Ubers / statline.Time.TotalMinutes;
                    statline.UbersDropped = player.Value.drops;
                    
                    // Logs from before mid 2014 do not always contain advanced med stats
                    try
                    {
                        statline.Medigun = player.Value.ubertypes.medigun;
                        statline.Kritzkrieg = player.Value.ubertypes.kritzkrieg;
                        statline.Vaccinator = player.Value.ubertypes.vaccinator;
                        statline.QuickFix = player.Value.ubertypes.quickfix;
                        statline.AdvantagesLost = player.Value.medicstats.advantages_lost;
                        statline.BiggestAdvantageLost = player.Value.medicstats.biggest_advantage_lost;
                        statline.DeathsAfterCharge = player.Value.medicstats.deaths_within_20s_after_uber;
                        statline.DeathsCloseToUber = player.Value.medicstats.deaths_with_95_99_uber;
                        statline.AvgTimeBeforeHealing = player.Value.medicstats.avg_time_before_healing;
                        statline.AvgBuildTime = player.Value.medicstats.avg_time_to_build;
                        statline.AvgTimeBeforeUsing = player.Value.medicstats.avg_time_before_using;
                        statline.AvgUberLength = player.Value.medicstats.avg_uber_length;
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine("Stats could not be processed and does not contain appropriate information: " + ex);
                    }
                }
                statlines.Add(statline);
            }
            foreach (Statline statline in statlines)
            {
                foreach(dynamic player in classKills)
                {
                    if (player.Name == statline.Steam3ID)
                    {
                        statline.ScoutKills = player.Value.scout;
                        statline.SoldierKills = player.Value.soldier;
                        statline.PyroKills = player.Value.pyro;
                        statline.DemomanKills = player.Value.demoman;
                        statline.HeavyKills = player.Value.heavyweapons;
                        statline.EngineerKills = player.Value.engineer;
                        statline.MedicKills = player.Value.medic;
                        statline.MedicPicks = player.Value.medic;
                        if (statline.MedicPicks != null)
                        {
                            statline.MedicPicksPerMinute = (double)player.Value.medic / statline.Time.TotalMinutes;
                        }
                        statline.SniperKills = player.Value.sniper;
                        statline.SpyKills = player.Value.spy;
                    }
                }
                foreach (dynamic player in classDeaths)
                {
                    if (player.Name == statline.Steam3ID)
                    {
                        statline.ScoutDeaths = player.Value.scout;
                        statline.SoldierDeaths = player.Value.soldier;
                        statline.PyroDeaths = player.Value.pyro;
                        statline.DemomanDeaths = player.Value.demoman;
                        statline.HeavyDeaths = player.Value.heavyweapons;
                        statline.EngineerDeaths = player.Value.engineer;
                        statline.MedicDeaths = player.Value.medic;
                        statline.SniperDeaths = player.Value.sniper;
                        statline.SpyDeaths = player.Value.spy;
                    }
                }
                foreach (dynamic medic in healSpread)
                {
                    if (statline.Class == "medic" && medic.Name == statline.Steam3ID)
                    {
                        foreach (Statline sl in statlines)
                        {
                            foreach (dynamic player in medic.Value)
                            {
                                if (player.Name == sl.Steam3ID)
                                {
                                    sl.HealsReceived = player.Value;
                                    sl.HealPercentageReceived = (double)sl.HealsReceived / (double)statline.Healing * 100.0;
                                }
                            }
                        }
                    }
                }
            }
            await _statlineRepo.AddStatlinesAsync(statlines);
            List<GroupedClassStatline> classStatlines = new List<GroupedClassStatline>();
            classStatlines = _statlineRepo.GetGroupedStatlines(statlines).ToList();

            // Delete log from log folder
            if (System.IO.File.Exists(@"logs\log_" + logID + ".log"))
            {
                System.IO.File.Delete(@"logs\log_" + logID + ".log");
            }

            //return classStatlines;
            return match;
        }

        public async Task<GroupedClassStatlineMatchModel> ParseLogs(List<string> logIDs, string userId = null, string connectionId = null)
        {
            // New statlines that have to be added to the database
            List<Statline> statlines = new List<Statline>();

            // Statlines that are already present in the database and do not need to be fetched again
            List<Statline> existingStatlines = new List<Statline>();

            List<Match> existingMatches = new List<Match>();

            // List of matches
            List<Match> matches = new List<Match>();

            // Create new HTTP client
            HttpClient client = new HttpClient();

            // Apply correct header
            client.DefaultRequestHeaders.Add("Accept", "application/json");

            // Loop through logIDs
            foreach (string logID in logIDs)
            {
                // Temporary statlines for match
                List<Statline> matchStatlines = new List<Statline>();

                // URI for zip archive
                string zipUrl = "http://logs.tf/logs/log_" + logID + ".log.zip";
                WebClient web = new WebClient();
                
                // Download zip and extract the log
                Stream data = web.OpenRead(new Uri(zipUrl));
                UnzipFromStream(data, @"logs");

                // Read all lines and place them in a list
                List<string> lines = System.IO.File.ReadAllLines(@"logs\log_" + logID + ".log").ToList();

                // Lines containing information regarding round length
                List<string> roundLines = lines.Where(s => s.Contains("World triggered \"Round_Length\"") || s.Contains("World triggered \"Mini_Round_Length\"")).ToList();

                // Lines regarding build events for sentries, dispensers and teleporters
                List<string> sentriesBuiltLines = lines.Where(s => s.Contains("triggered \"player_builtobject\" (object \"OBJ_SENTRYGUN\")")).ToList();
                List<string> dispensersBuiltLines = lines.Where(s => s.Contains("triggered \"player_builtobject\" (object \"OBJ_DISPENSER\")")).ToList();
                List<string> teleportersBuiltLines = lines.Where(s => s.Contains("triggered \"player_builtobject\" (object \"OBJ_TELEPORTER\")")).ToList();

                // Older logs have a different way of checking for buildings built
                List<string> sentriesBuiltLinesOld = lines.Where(s => s.Contains("triggered \"builtobject\" (object \"OBJ_SENTRYGUN\")")).ToList();
                List<string> dispensersBuiltLinesOld = lines.Where(s => s.Contains("triggered \"builtobject\" (object \"OBJ_DISPENSER\")")).ToList();
                List<string> teleportersBuiltLinesOld = lines.Where(s => s.Contains("triggered \"builtobject\" (object \"OBJ_TELEPORTER\")")).ToList();

                // Lines regarding destruction of sentries, dispensers and teleporters
                List<string> sentriesDestroyedLines = lines.Where(s => s.Contains("triggered \"killedobject\" (object \"OBJ_SENTRYGUN\")") && !s.Contains("(weapon \"pda_engineer\")")).ToList();
                List<string> dispensersDestroyedLines = lines.Where(s => s.Contains("triggered \"killedobject\" (object \"OBJ_DISPENSER\")") && !s.Contains("(weapon \"pda_engineer\")")).ToList();
                List<string> teleportersDestroyedLines = lines.Where(s => s.Contains("triggered \"killedobject\" (object \"OBJ_TELEPORTER\")") && !s.Contains("(weapon \"pda_engineer\")")).ToList();

                // Lines regarding extinguishes
                List<string> pyroExtinguishes = lines.Where(s => s.Contains("triggered \"player_extinguished\"")).ToList();

                // Calculate the total time of the match
                double totalTime = 0.0;
                foreach (string line in roundLines)
                {
                    // Split based on " and pick the 3rd element of the array to obtain the value
                    string[] roundTime = line.Split("\"");
                    // Convert string into double
                    double time = double.Parse(roundTime[3], System.Globalization.CultureInfo.InvariantCulture);
                    // Add round time to total time
                    totalTime += time;
                }

                // Add log id
                string url = "https://logs.tf/json/" + logID;
                
                // Fetch json log
                var response = await client.GetStringAsync(url);
                
                // Parse log into JObject
                dynamic log = JObject.Parse(response);
                
                // Make new match
                Match match = new Match();

                // Map (as described on logs.tf, does not contain the actual map played (TODO))
                match.Map = log.info.map;
                // Log name
                match.Name = log.info.title;
                // DateTime of when the match was played
                match.Date = UnixTimeStampToDateTime(Convert.ToDouble((log.info.date)));
                // Total duration of the match
                match.Time = TimeSpan.FromSeconds(Convert.ToDouble(log.length));
                // Actual playtime (pauses removed)
                if (totalTime != 0.0)
                {
                    match.PlayTime = TimeSpan.FromSeconds(Math.Round(totalTime, 0));
                }
                else
                {
                    // In some VERY rare situations, logs do not contain the proper time played. In this case, resort back to the regular time.
                    match.PlayTime = TimeSpan.FromSeconds(Convert.ToDouble(log.length));
                }
                
                // Logs.tf ID
                match.LogID = logID;

                // Check if there is a matching log with said ID
                Match matchPresent = await _matchRepo.FindByLogIdAsync(logID);

                // If present, fetch statlines and add to existingStatlines (no need to process log again)
                if (matchPresent != null)
                {
                    // Replace current match with match stored in db
                    match = matchPresent;
                    // Get the statlines present in the db
                    IEnumerable<Statline> statlinesPresent = await _statlineRepo.GetStatlinesFromMatch(matchPresent);
                    // Add statlines to existingStatlines
                    existingStatlines.AddRange(statlinesPresent);
                    existingMatches.Add(matchPresent);
                    //Debug.WriteLine("Match with LogID " + match.LogID +  " already present in database.");
                    //if (userId != null)
                    //{
                    //    await _hubContext.Clients.User(userId).SendAsync("SendProgressUpdate", "Processing " + match.Name + " (" + match.Map + ")" + "...", userId);
                    //}
                }
                else
                {
                    // If match does not exist, add to database
                    match = await _matchRepo.AddMatchAsync(match);
                    //Debug.WriteLine("Add match with LogID " + match.LogID + " to the database.");

                    int index = logIDs.FindIndex(l => l == logID);

                    // General player stats
                    dynamic players = log.players;

                    // Kills gotten on certain classes
                    dynamic classKills = log.classkills;

                    // Deaths to certain classes
                    dynamic classDeaths = log.classdeaths;

                    // Heals received
                    dynamic healSpread = log.healspread;

                    // Loop through individual player stats
                    foreach (dynamic player in players)
                    {
                        // Create new statline
                        Statline statline = new Statline();
                        // Steam3ID
                        statline.Steam3ID = player.Name;
                        //Debug.WriteLine("Processing statline for player with SteamID " + statline.Steam3ID);

                        // Establish relation with match
                        statline.MatchID = match.ID;

                        // Store log id for easy reference
                        statline.LogID = logID;

                        try
                        {
                            // Fetch ETF2L info
                            string profileETF2L = "http://api.etf2l.org/player/" + statline.Steam3ID;
                            var responseETF2L = await client.GetStringAsync(profileETF2L);
                            dynamic infoETF2L = JObject.Parse(responseETF2L);
                            dynamic status = infoETF2L.status.code;
                            // If successful, get user alias
                            if (status == 200)
                            {
                                statline.Alias = infoETF2L.player.name;
                            }
                        }
                        catch (HttpRequestException ex)
                        {
                            // If not successful, use logs.tf name as fallback
                            Debug.WriteLine($"An error occurred fetching the player's ETF2L alias: {ex}");
                            dynamic names = log.names;

                            foreach (dynamic name in names)
                            {
                                if (name.Name == statline.Steam3ID)
                                {
                                    statline.Alias = name.Value;
                                }
                            }
                        }

                        // Sentries, dispensers and teles constructed
                        int sentriesBuilt = 0;
                        int dispensersBuilt = 0;
                        int teleportersBuilt = 0;

                        // Sentries, dispensers and teles destroyed
                        int sentriesDestroyed = 0;
                        int dispensersDestroyed = 0;
                        int teleportersDestroyed = 0;

                        // Extinguishes
                        int extinguishes = 0;

                        // Check for SteamID in lines, if present: add to relevant building

                        foreach (string line in sentriesBuiltLines)
                        {
                            if (line.Contains(statline.Steam3ID))
                            {
                                sentriesBuilt++;
                            }
                        }

                        foreach (string line in dispensersBuiltLines)
                        {
                            if (line.Contains(statline.Steam3ID))
                            {
                                dispensersBuilt++;
                            }
                        }

                        foreach (string line in teleportersBuiltLines)
                        {
                            if (line.Contains(statline.Steam3ID))
                            {
                                teleportersBuilt++;
                            }
                        }

                        foreach (string line in sentriesBuiltLinesOld)
                        {
                            if (line.Contains(statline.Steam3ID))
                            {
                                sentriesBuilt++;
                            }
                        }

                        foreach (string line in dispensersBuiltLinesOld)
                        {
                            if (line.Contains(statline.Steam3ID))
                            {
                                dispensersBuilt++;
                            }
                        }

                        foreach (string line in teleportersBuiltLinesOld)
                        {
                            if (line.Contains(statline.Steam3ID))
                            {
                                teleportersBuilt++;
                            }
                        }

                        // Check for first SteamID in line, if present: add to relevant building destroyed
                        foreach (string line in sentriesDestroyedLines)
                        {
                            string steamid = line.Split("><")[1];
                            if (steamid == statline.Steam3ID)
                            {
                                sentriesDestroyed++;
                            }
                        }

                        foreach (string line in dispensersDestroyedLines)
                        {
                            string steamid = line.Split("><")[1];
                            if (steamid == statline.Steam3ID)
                            {
                                dispensersDestroyed++;
                            }
                        }

                        foreach (string line in teleportersDestroyedLines)
                        {
                            string steamid = line.Split("><")[1];
                            if (steamid == statline.Steam3ID)
                            {
                                teleportersDestroyed++;
                            }
                        }

                        // Generic stats
                        dynamic regularStats = player.Value.class_stats[0];

                        // Total time
                        if (Convert.ToDouble(regularStats.total_time) != 0.0)
                        {
                            statline.Time = TimeSpan.FromSeconds(Convert.ToDouble(regularStats.total_time));
                        }
                        else
                        {
                            try
                            {
                                statline.Time = match.PlayTime;
                                Debug.WriteLine("Warning: a statline was found for player " + statline.Alias + " with 0 time registered. Assigned overall match time as statline time.");
                            }
                            catch (Exception ex)
                            {
                                statline.Time = TimeSpan.FromSeconds(1.0);
                                Debug.WriteLine("Warning: a statline was found for player " + statline.Alias + " with 0 time registered. Manually adjust time in the database." + ex);
                            }
                        }

                        // If a player has more playtime than the total round time: correct to get rid of excess time due to pauses
                        if (statline.Time > match.PlayTime)
                        {
                            statline.Time = match.PlayTime;
                        }

                        // Class played
                        statline.Class = regularStats.type;

                        // Count extinguishes for pyro players (medic can also extinguish but is not factored in)
                        foreach (string line in pyroExtinguishes)
                        {
                            string steamid = line.Split("><")[1];
                            if (steamid == statline.Steam3ID && statline.Class == "pyro")
                            {
                                extinguishes++;
                            }
                        }

                        // Absolute stats
                        statline.Kills = regularStats.kills;
                        statline.Assists = regularStats.assists;
                        statline.Deaths = regularStats.deaths;

                        // Logs.tf has a weird issue where damage for class stats sometimes exceeds total damage (???)
                        // https://puu.sh/Cpjmh.png
                        //statline.Damage = regularStats.dmg;
                        if (regularStats.dmg > player.Value.dmg)
                        {
                            statline.Damage = player.Value.dmg;
                        }
                        else
                        {
                            statline.Damage = regularStats.dmg;
                        }

                        // Logs from around the 2013 era do not contain airshots hit.
                        try
                        {
                            statline.Airshots = player.Value.@as;
                        }
                        catch (Exception ex)
                        {
                            Debug.WriteLine("Old log: no airshots processed for this player." + ex);
                        }

                        // Logs from around the 2013 era may or may not contain damage taken.
                        try
                        {
                            statline.DamageTaken = player.Value.dt;
                        }
                        catch (Exception ex)
                        {
                            Debug.WriteLine("Old log: no airshots processed for this player." + ex);
                        }
                        
                        statline.Caps = player.Value.cpc;
                        statline.Suicides = player.Value.suicides;
                        statline.HealthPacks = player.Value.medkits;
                        statline.SentriesDestroyed = sentriesDestroyed;
                        statline.DispensersDestroyed = dispensersDestroyed;
                        statline.TeleportersDestroyed = teleportersDestroyed;

                        // Stats based on time
                        statline.DeathsPerMinute = (double)statline.Deaths / statline.Time.TotalMinutes;
                        statline.KillsPerMinute = (double)statline.Kills / statline.Time.TotalMinutes;
                        statline.AssistsPerMinute = (double)statline.Assists / statline.Time.TotalMinutes;
                        statline.DamagePerMinute = (double)statline.Damage / statline.Time.TotalMinutes;

                        // Logs from the 2013 era may or may not contain damage taken. If not, ignore.
                        try
                        {
                            statline.DamageTakenPerMinute = (double)statline.DamageTaken / statline.Time.TotalMinutes;
                        }
                        catch (Exception ex)
                        {
                            Debug.WriteLine("Cannot calculate damage taken per minute: old log." + ex);
                        }

                        // Airshots are not listed in 2013 era logs.
                        try
                        {
                            statline.AirshotsPerMinute = (double)statline.Airshots / statline.Time.TotalMinutes;
                        } catch (Exception ex)
                        {
                            Debug.WriteLine("Cannot calculate airshots per minute: old log." + ex);
                        }

                        statline.KillsAssistsPerMinute = ((double)statline.Kills + (double)statline.Assists) / statline.Time.TotalMinutes;

                        // Pyro specific stats
                        if (statline.Class == "pyro")
                        {
                            statline.Extinguishes = extinguishes;
                            statline.ExtinguishesPerMinute = (double)statline.Extinguishes / statline.Time.TotalMinutes;
                        }

                        // Stats based on deaths
                        if (statline.Deaths != 0)
                        {
                            statline.KillsPerDeaths = (double)statline.Kills / (double)statline.Deaths;
                            statline.KillsAssistsPerDeaths = ((double)statline.Kills + (double)statline.Assists) / (double)statline.Deaths;
                        }

                        // Headshot stats (spy/sniper, let's not count reflected huntsman arrows please ty)
                        if (statline.Class == "spy" || statline.Class == "sniper")
                        {
                            // Old logs only count headshot kills and not individual headshots.
                            try
                            {
                                statline.Headshots = player.Value.headshots_hit;
                                statline.HeadshotsPerMinute = (double)statline.Headshots / statline.Time.TotalMinutes;
                            }
                            catch (Exception ex)
                            {
                                Debug.WriteLine("Could not process headshots: old log. " + ex);
                            }
                        }

                        // Backstabs
                        if (statline.Class == "spy")
                        {
                            statline.Backstabs = player.Value.backstabs;
                            statline.BackstabsPerMinute = (double)statline.Backstabs / statline.Time.TotalMinutes;
                        }

                        // Total healing done (relevant for medic and to some extent soldier with conch)
                        if (statline.Class == "medic" || statline.Class == "soldier")
                        {
                            statline.Healing = player.Value.heal;
                            statline.HealingPerMinute = (double)statline.Healing / statline.Time.TotalMinutes;
                        }

                        // Medic stats
                        if (statline.Class == "medic")
                        {
                            statline.Ubers = player.Value.ubers;
                            statline.UbersPerMinute = statline.Ubers / statline.Time.TotalMinutes;
                            statline.UbersDropped = player.Value.drops;

                            // Logs from era 2014 and older might not support advanced medic stats.

                            try
                            {
                                statline.Medigun = player.Value.ubertypes.medigun;
                                statline.Kritzkrieg = player.Value.ubertypes.kritzkrieg;
                                statline.Vaccinator = player.Value.ubertypes.vaccinator;
                                statline.QuickFix = player.Value.ubertypes.quickfix;
                                statline.AdvantagesLost = player.Value.medicstats.advantages_lost;
                                statline.BiggestAdvantageLost = player.Value.medicstats.biggest_advantage_lost;
                                statline.DeathsAfterCharge = player.Value.medicstats.deaths_within_20s_after_uber;
                                statline.DeathsCloseToUber = player.Value.medicstats.deaths_with_95_99_uber;
                                statline.AvgTimeBeforeHealing = player.Value.medicstats.avg_time_before_healing;
                                statline.AvgBuildTime = player.Value.medicstats.avg_time_to_build;
                                statline.AvgTimeBeforeUsing = player.Value.medicstats.avg_time_before_using;
                                statline.AvgUberLength = player.Value.medicstats.avg_uber_length;
                            }
                            catch (Exception ex)
                            {
                                Debug.WriteLine("Stats could not be processed and does not contain appropriate information: " + ex);
                            }                   
                        }

                        // Buildings built
                        if (statline.Class == "engineer")
                        {
                            statline.SentriesBuilt = sentriesBuilt;
                            statline.DispensersBuilt = dispensersBuilt;
                            statline.TeleportersBuilt = teleportersBuilt;
                        }

                        // Save statline to temporary list
                        matchStatlines.Add(statline);
                    }

                    // Loop through statlines
                    foreach (Statline statline in matchStatlines)
                    {
                        // Class specific kills
                        foreach (dynamic player in classKills)
                        {
                            if (player.Name == statline.Steam3ID)
                            {
                                statline.ScoutKills = player.Value.scout;
                                statline.SoldierKills = player.Value.soldier;
                                statline.PyroKills = player.Value.pyro;
                                statline.DemomanKills = player.Value.demoman;
                                statline.HeavyKills = player.Value.heavyweapons;
                                statline.EngineerKills = player.Value.engineer;
                                statline.MedicKills = player.Value.medic;
                                statline.MedicPicks = player.Value.medic;
                                if (statline.MedicPicks != null)
                                {
                                    statline.MedicPicksPerMinute = (double)player.Value.medic / statline.Time.TotalMinutes;
                                }
                                statline.SniperKills = player.Value.sniper;
                                statline.SpyKills = player.Value.spy;
                            }
                        }

                        // Class specific deaths
                        foreach (dynamic player in classDeaths)
                        {
                            if (player.Name == statline.Steam3ID)
                            {
                                statline.ScoutDeaths = player.Value.scout;
                                statline.SoldierDeaths = player.Value.soldier;
                                statline.PyroDeaths = player.Value.pyro;
                                statline.DemomanDeaths = player.Value.demoman;
                                statline.HeavyDeaths = player.Value.heavyweapons;
                                statline.EngineerDeaths = player.Value.engineer;
                                statline.MedicDeaths = player.Value.medic;
                                statline.SniperDeaths = player.Value.sniper;
                                statline.SpyDeaths = player.Value.spy;
                            }
                        }

                        // Heal spread + average heal percentage received
                        foreach (dynamic medic in healSpread)
                        {
                            if (statline.Class == "medic" && medic.Name == statline.Steam3ID)
                            {
                                foreach (Statline sl in matchStatlines)
                                {
                                    foreach (dynamic player in medic.Value)
                                    {
                                        if (player.Name == sl.Steam3ID)
                                        {
                                            sl.HealsReceived = player.Value;
                                            sl.HealPercentageReceived = (double)sl.HealsReceived / (double)statline.Healing * 100.0;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    // Add matchStatlines to list of statlines (aka new statlines that have not been added yet)
                    statlines.AddRange(matchStatlines);
                    
                    // Add match to list of matches 
                    matches.Add(match);
                }
            }
            // Asynchronously add statlines to db
            Debug.WriteLine(statlines.ToString());
            Debug.WriteLine(JsonConvert.SerializeObject(statlines));
            statlines = (await _statlineRepo.AddStatlinesAsync(statlines)).ToList();
            
            // Process statlines per player, per class
            List<GroupedClassStatline> classStatlines = new List<GroupedClassStatline>();

            // Unite existing and new statlines into one list
            List<Statline> allStatlines = statlines;
            allStatlines.AddRange(existingStatlines);
            List<Match> allMatches = matches;
            allMatches.AddRange(existingMatches);

            // Aggregate stats
            classStatlines = _statlineRepo.GetGroupedStatlines(allStatlines).ToList();

            // Make model containing relevant matches and processed statlines
            GroupedClassStatlineMatchModel gcsmm = new GroupedClassStatlineMatchModel();
            gcsmm.Matches = allMatches;
            gcsmm.GroupedClassStatlines = classStatlines;

            return gcsmm;
        }

        public void UnzipFromStream(Stream zipStream, string outFolder)
        {
            ZipInputStream zipInputStream = new ZipInputStream(zipStream);
            ZipEntry zipEntry = zipInputStream.GetNextEntry();

            while (zipEntry != null)
            {
                String entryFileName = zipEntry.Name;
                byte[] buffer = new byte[4096];

                String fullZipToPath = Path.Combine(outFolder, entryFileName);
                string directoryName = Path.GetDirectoryName(fullZipToPath);
                if (directoryName.Length > 0)
                    Directory.CreateDirectory(directoryName);

                string fileName = Path.GetFileName(fullZipToPath);

                if (fileName.Length == 0)
                {
                    zipEntry = zipInputStream.GetNextEntry();
                    continue;
                }

                using (FileStream streamWriter = File.Create(fullZipToPath))
                {
                    StreamUtils.Copy(zipInputStream, streamWriter, buffer);
                }

                zipEntry = zipInputStream.GetNextEntry();
            }
        }

        public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }
    }
}
