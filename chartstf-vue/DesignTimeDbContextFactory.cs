using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace chartstf
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<LogAPIContext>
    {
        public LogAPIContext CreateDbContext(string[] args)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            var builder = new DbContextOptionsBuilder<LogAPIContext>();

            var connectionString = configuration.GetConnectionString("LogAPIContext");

            builder.UseMySql(connectionString);

            return new LogAPIContext(builder.Options);
        }
    }
}
