﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace chartstf_vue.Migrations
{
    public partial class Logs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    SteamId = table.Column<ulong>(nullable: false),
                    Steam3ID = table.Column<string>(nullable: true),
                    PersonaName = table.Column<string>(nullable: true),
                    AvatarFull = table.Column<string>(nullable: true),
                    AvatarMedium = table.Column<string>(nullable: true),
                    AvatarSmall = table.Column<string>(nullable: true),
                    ETF2LUrl = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                    table.UniqueConstraint("AK_AspNetUsers_SteamId", x => x.SteamId);
                });

            migrationBuilder.CreateTable(
                name: "Matches",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Map = table.Column<string>(nullable: true),
                    LogID = table.Column<string>(maxLength: 20, nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    Time = table.Column<TimeSpan>(nullable: false),
                    PlayTime = table.Column<TimeSpan>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Matches", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Players",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 70, nullable: false),
                    SteamID64 = table.Column<int>(nullable: false),
                    Steam3ID = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Players", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Teams",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    ETF2LID = table.Column<int>(maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Teams", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    RoleId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Collections",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 200, nullable: false),
                    ShortUrl = table.Column<string>(nullable: true),
                    IsPrivate = table.Column<bool>(nullable: true),
                    UserId = table.Column<string>(nullable: true),
                    SteamId = table.Column<ulong>(nullable: false),
                    SteamUserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Collections", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Collections_AspNetUsers_SteamUserId",
                        column: x => x.SteamUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Statlines",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    MatchID = table.Column<Guid>(nullable: false),
                    PlayerID = table.Column<Guid>(nullable: false),
                    LogID = table.Column<string>(nullable: true),
                    SteamID64 = table.Column<int>(nullable: false),
                    Steam3ID = table.Column<string>(nullable: false),
                    Class = table.Column<string>(nullable: false),
                    Alias = table.Column<string>(nullable: true),
                    Kills = table.Column<int>(nullable: false),
                    KillsPerMinute = table.Column<double>(nullable: false),
                    KillsAssistsPerMinute = table.Column<double>(nullable: false),
                    KillsPerDeaths = table.Column<double>(nullable: true),
                    KillsAssistsPerDeaths = table.Column<double>(nullable: true),
                    Deaths = table.Column<int>(nullable: false),
                    DeathsPerMinute = table.Column<double>(nullable: false),
                    Damage = table.Column<int>(nullable: false),
                    DamagePerMinute = table.Column<double>(nullable: false),
                    Assists = table.Column<int>(nullable: false),
                    AssistsPerMinute = table.Column<double>(nullable: false),
                    Healing = table.Column<int>(nullable: true),
                    HealingPerMinute = table.Column<double>(nullable: true),
                    Ubers = table.Column<int>(nullable: true),
                    UbersPerMinute = table.Column<double>(nullable: true),
                    UbersDropped = table.Column<int>(nullable: true),
                    Medigun = table.Column<int>(nullable: true),
                    Kritzkrieg = table.Column<int>(nullable: true),
                    Vaccinator = table.Column<int>(nullable: true),
                    QuickFix = table.Column<int>(nullable: true),
                    AvgBuildTime = table.Column<double>(nullable: true),
                    AvgTimeBeforeUsing = table.Column<double>(nullable: true),
                    AdvantagesLost = table.Column<int>(nullable: true),
                    BiggestAdvantageLost = table.Column<int>(nullable: true),
                    DeathsCloseToUber = table.Column<int>(nullable: true),
                    AvgUberLength = table.Column<double>(nullable: true),
                    AvgTimeBeforeHealing = table.Column<double>(nullable: true),
                    DeathsAfterCharge = table.Column<int>(nullable: true),
                    Caps = table.Column<int>(nullable: true),
                    Suicides = table.Column<int>(nullable: true),
                    HealthPacks = table.Column<int>(nullable: true),
                    Backstabs = table.Column<int>(nullable: true),
                    BackstabsPerMinute = table.Column<double>(nullable: true),
                    Headshots = table.Column<int>(nullable: true),
                    HeadshotsPerMinute = table.Column<double>(nullable: true),
                    Airshots = table.Column<int>(nullable: true),
                    AirshotsPerMinute = table.Column<double>(nullable: true),
                    MedicPicks = table.Column<int>(nullable: true),
                    MedicPicksPerMinute = table.Column<double>(nullable: true),
                    DamageTaken = table.Column<int>(nullable: false),
                    DamageTakenPerMinute = table.Column<double>(nullable: false),
                    HealsReceived = table.Column<int>(nullable: true),
                    HealPercentageReceived = table.Column<double>(nullable: true),
                    Time = table.Column<TimeSpan>(nullable: false),
                    ScoutKills = table.Column<int>(nullable: true),
                    SoldierKills = table.Column<int>(nullable: true),
                    PyroKills = table.Column<int>(nullable: true),
                    DemomanKills = table.Column<int>(nullable: true),
                    HeavyKills = table.Column<int>(nullable: true),
                    EngineerKills = table.Column<int>(nullable: true),
                    MedicKills = table.Column<int>(nullable: true),
                    SniperKills = table.Column<int>(nullable: true),
                    SpyKills = table.Column<int>(nullable: true),
                    ScoutDeaths = table.Column<int>(nullable: true),
                    SoldierDeaths = table.Column<int>(nullable: true),
                    PyroDeaths = table.Column<int>(nullable: true),
                    DemomanDeaths = table.Column<int>(nullable: true),
                    HeavyDeaths = table.Column<int>(nullable: true),
                    EngineerDeaths = table.Column<int>(nullable: true),
                    MedicDeaths = table.Column<int>(nullable: true),
                    SniperDeaths = table.Column<int>(nullable: true),
                    SpyDeaths = table.Column<int>(nullable: true),
                    SentriesBuilt = table.Column<int>(nullable: true),
                    DispensersBuilt = table.Column<int>(nullable: true),
                    TeleportersBuilt = table.Column<int>(nullable: true),
                    SentriesDestroyed = table.Column<int>(nullable: true),
                    DispensersDestroyed = table.Column<int>(nullable: true),
                    TeleportersDestroyed = table.Column<int>(nullable: true),
                    Extinguishes = table.Column<int>(nullable: true),
                    ExtinguishesPerMinute = table.Column<double>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Statlines", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Statlines_Matches_MatchID",
                        column: x => x.MatchID,
                        principalTable: "Matches",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CollectionsMatches",
                columns: table => new
                {
                    MatchID = table.Column<Guid>(nullable: false),
                    CollectionID = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CollectionsMatches", x => new { x.MatchID, x.CollectionID });
                    table.ForeignKey(
                        name: "FK_CollectionsMatches_Collections_CollectionID",
                        column: x => x.CollectionID,
                        principalTable: "Collections",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CollectionsMatches_Matches_MatchID",
                        column: x => x.MatchID,
                        principalTable: "Matches",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Collections_SteamUserId",
                table: "Collections",
                column: "SteamUserId");

            migrationBuilder.CreateIndex(
                name: "IX_CollectionsMatches_CollectionID",
                table: "CollectionsMatches",
                column: "CollectionID");

            migrationBuilder.CreateIndex(
                name: "IX_Statlines_MatchID",
                table: "Statlines",
                column: "MatchID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "CollectionsMatches");

            migrationBuilder.DropTable(
                name: "Players");

            migrationBuilder.DropTable(
                name: "Statlines");

            migrationBuilder.DropTable(
                name: "Teams");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "Collections");

            migrationBuilder.DropTable(
                name: "Matches");

            migrationBuilder.DropTable(
                name: "AspNetUsers");
        }
    }
}
