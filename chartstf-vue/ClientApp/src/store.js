import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

/* eslint-disable */
export default new Vuex.Store({
    state: {
        collections: [],
        userCollections: [],
        collectionUrl: '',
        identifier: '',
        stats: []
    },
    getters: {
        // Data
        getCollections: state => state.collections,
        getStats: state => state.stats,

        // Classes
        getScouts: state => state.stats.filter(stat => stat.class == 'scout'),
        getSoldiers: state => state.stats.filter(stat => stat.class == 'soldier'),
        getPyros: state => state.stats.filter(stat => stat.class == 'pyro'),
        getDemomen: state => state.stats.filter(stat => stat.class == 'demoman'),
        getHeavies: state => state.stats.filter(stat => stat.class == 'heavyweapons'),
        getEngineers: state => state.stats.filter(stat => stat.class == 'engineer'),
        getMedics: state => state.stats.filter(stat => stat.class == 'medic'),
        getSnipers: state => state.stats.filter(stat => stat.class == 'sniper'),
        getSpies: state => state.stats.filter(stat => stat.class == 'spy'),
    },
    mutations: {
        setCollections(state, c) {
            state.collections = c
        },
        setUserCollections(state, c) {
            state.userCollections = c
        },
        setStats(state, s) {
            state.stats = s
        }
    },
    actions: {
        fetchCollections: ({ commit }) => {
            return new Promise((resolve, reject) => {
                axios.get(`/api/collections`)
                    .then(response => {
                        commit('setCollections', response.data);
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
            })
        },
        fetchStats: ({ commit }, shortUrl) => {
            return new Promise((resolve, reject) => {
                axios.get(`/api/collections/` + shortUrl)
                    .then(response => {
                        commit('setStats', response.data);
                        resolve(response);
                    })
                    .catch(error => {
                        reject(error)
                    })
            })
        }
    }
})
