import Vue from 'vue'
import axios from 'axios'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'
import Buefy from 'buefy'
import VueAnalytics from 'vue-analytics'

import { dom, library } from '@fortawesome/fontawesome-svg-core'
import { faBars, faPlus, faAngleUp, faAngleDown, faAngleLeft, faAngleRight } from '@fortawesome/free-solid-svg-icons'
import { faSteam } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon, FontAwesomeLayers } from '@fortawesome/vue-fontawesome'

Vue.prototype.$http = axios

library.add({ faBars, faPlus, faSteam, faAngleUp, faAngleDown, faAngleLeft, faAngleRight })

dom.watch()
Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.component('font-awesome-layers', FontAwesomeLayers)
Vue.use(Buefy, {
    defaultIconPack: 'fa',
    defaultContainerElement: '#content'
})

Vue.use(VueAnalytics, {
    id: 'UA-138912074-1',
    router
})

Vue.config.productionTip = false

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')
