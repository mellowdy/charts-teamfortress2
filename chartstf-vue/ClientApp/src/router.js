import Vue from "vue";
import Router from "vue-router";
import HomePage from "../src/views/HomePage.vue";
import About from "../src/views/About.vue";
import Collection from "../src/views/Collection.vue";
import Gallery from "../src/views/Gallery.vue";

Vue.use(Router);

export default new Router({
    mode: "history",
    base: process.env.BASE_URL,
    routes: [
        { name: "home", path: "/", component: HomePage, display: "Home" },
        { name: "about", path: "/about", component: About, display: "About" },
        {
            name: "collection",
            path: "/collection/:id",
            component: Collection,
            display: "Collection"
        },
        {
            name: "gallery",
            path: "/gallery",
            component: Gallery,
            display: "Gallery"
        }
    ]
});
