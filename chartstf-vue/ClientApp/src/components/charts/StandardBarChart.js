import { Bar } from 'vue-chartjs'

export default {
  extends: Bar,
  props: {
    data: {
      type: Object,
      default: null
    }
  },
  mounted() {
    this.renderChart(this.data, {
      responsive: true,
      maintainAspectRatio: false,
      scales: {
        yAxes: [
          {
            display: false,
            stacked: false
          }
        ],
        xAxes: [
          {
            categoryPercentage: 0.5,
            barPercentage: 1
          }
        ]
      },
      plugins: {
        deferred: {
          enabled: true,
          delay: 300,
          xOffset: '50%'
        }
      }
    })
  }
}
