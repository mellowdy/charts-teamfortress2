using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using Microsoft.AspNetCore.Cors;
using Microsoft.EntityFrameworkCore;

namespace chartstf
{
    [Authorize]
    [Route("api/collections")]
    [ApiController]
    public class CollectionController : ControllerBase
    {
        private readonly LogAPIContext _context;
        private readonly IMapper _mapper;
        private readonly IUrlHelper _urlHelper;
        private readonly ILogger<CollectionController> _logger;
        private readonly UserManager<SteamUser> _userManager;

        private IMatchRepo _matchRepo;
        private ICollectionRepo _collectionRepo;
        private IStatlineRepo _statlineRepo;
        private ICollectionsMatchesRepo _collectionsMatchesRepo;
        private ILogService _logService;

        public CollectionController(LogAPIContext context, IMapper mapper, IUrlHelper urlHelper, ILogger<CollectionController> logger, IMatchRepo matchRepo, ICollectionsMatchesRepo collectionsMatchesRepo, ICollectionRepo collectionRepo, IStatlineRepo statlineRepo, ILogService logRepo, UserManager<SteamUser> userManager)
        {
            this._context = context;
            this._mapper = mapper;
            this._urlHelper = urlHelper;
            this._logger = logger;
            this._matchRepo = matchRepo;
            this._collectionRepo = collectionRepo;
            this._statlineRepo = statlineRepo;
            this._logService = logRepo;
            this._collectionsMatchesRepo = collectionsMatchesRepo;
            this._userManager = userManager;
        }

        // GET: api/collections
        [AllowAnonymous]
        [HttpGet(Name = "GetCollections")]
        public async Task<IActionResult> GetCollections()
        {
            var collections = await _collectionRepo.GetPublicAsync();
            var collectionModels = _mapper.Map<IEnumerable<CollectionModel>>(collections);
            return Ok(collectionModels);
        }

        // GET: api/collections/{shortUrl}
        [AllowAnonymous]
        [HttpGet("{shortUrl}", Name = "GetCollection")]
        public async Task<IActionResult> GetCollection([FromRoute] string shortUrl)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var collection = await _collectionRepo.FindByShortUrlAsync(shortUrl);

                if (collection == null)
                {
                    return NotFound();
                }

                IEnumerable<GroupedClassStatline> classStatlines = await _statlineRepo.GetGroupedStatlinesFromCollection(collection.ID);
                return Ok(classStatlines.OrderBy(s => s.Alias));
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown when requesting collection: {ex}");
                return BadRequest("Failed to request collection");
            }
        }

        //[Authorize]
        [AllowAnonymous]
        [HttpPost(Name = "PostCollection")]
        public async Task<IActionResult> PostCollection([FromBody] CollectionModelForPost collectionModelPost)
        {
            if (collectionModelPost == null)
            {
                return BadRequest("Insufficient data.");
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (await _collectionRepo.CollectionExistsAsync(collectionModelPost.Name))
            {
                return StatusCode(409, "Name Conflict.");
            }

            Collection collection = _mapper.Map<Collection>(collectionModelPost);

            List<Match> matches = new List<Match>();
            List<CollectionsMatches> collectionsMatches = new List<CollectionsMatches>();

            try
            {
                GroupedClassStatlineMatchModel gcsmm = new GroupedClassStatlineMatchModel();

                gcsmm = await _logService.ParseLogs(collectionModelPost.Matches.ToList());

                matches = gcsmm.Matches;

                collection = await _collectionRepo.AddCollectionAsync(collection);
                collection = await _collectionRepo.GenerateShortUrl(collection);

                foreach (Match match in matches)
                {
                    CollectionsMatches cm = new CollectionsMatches { CollectionID = collection.ID, MatchID = match.ID };
                    collectionsMatches.Add(cm);
                    //Debug.WriteLine("Made relation between log " + match.LogID + " and collection with ID " + collection.ID);
                }

                await _collectionsMatchesRepo.AddCollectionsMatchesAsync(collectionsMatches);

                collection = await _collectionRepo.FindByIdAsync(collection.ID);
                CollectionModel collectionModel = _mapper.Map<CollectionModel>(collection);

                return Ok(gcsmm.GroupedClassStatlines.OrderBy(s => s.Alias));
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown when creating collection: {ex}");
                throw new Exception($"Failed to add collection with name \"{collection.Name}\" to the database.");
            }
        }

        // DELETE: api/Collection/5
        [Authorize(Roles = "Admin")]
        [HttpDelete("{id}")]
        public async Task<ActionResult<Collection>> DeleteCollection(Guid id)
        {
            var collection = await _collectionRepo.FindByIdAsync(id);

            if (collection == null)
            {
                return NotFound();
            }

            await _collectionRepo.DeleteCollectionAsync(collection);
            return collection;
        }
    }
}
