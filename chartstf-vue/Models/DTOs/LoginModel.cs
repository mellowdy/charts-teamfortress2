using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace chartstf
{
    public class LoginModel
    {
        [Required]
        public ulong SteamID { get; set; }
    }
}
