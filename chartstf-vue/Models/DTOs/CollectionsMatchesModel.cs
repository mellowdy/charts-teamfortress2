using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace chartstf
{
    public class CollectionsMatchesModel
    {
        public CollectionsMatchesModel(string collectionName)
        {
            this.CollectionName = collectionName;
        }

        public string LogID { get; set; }
        public string CollectionName { get; set; }
    }
}
