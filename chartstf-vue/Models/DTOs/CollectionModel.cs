using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace chartstf
{
    public class CollectionModel
    {
        public string Name { get; set; }
        public string ShortUrl { get; set; }
        public string[] Matches { get; set; }
    }
}
