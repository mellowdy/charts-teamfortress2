﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace chartstf
{
    public class MatchModel
    {
        public string Name { get; set; }
        public string Map { get; set; }
        public string LogID { get; set; }
        public DateTime Date { get; set; }
        public TimeSpan Time { get; set; }
        public string[] Collections { get; set; }
    }
}
