using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace chartstf
{
    public class CollectionModelForPost
    {
        public string Name { get; set; }
        public string[] Matches { get; set; }
        public bool IsPrivate { get; set; }
    }
}
