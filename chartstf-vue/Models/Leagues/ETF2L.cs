using System;
using System.Collections.Generic;
using System.Text;

namespace chartstf
{
    public class ETF2L
    {
        // ETF2L Team ID
        public int ID { get; set; }
        // Team name
        public string Name { get; set; }
        // Country flag
        public string Country { get; set; }
        // Abbreviation/tag
        public string Tag { get; set; }
        // Gamemode type
        public string Gamemode { get; set; }
        // Team picture
        public string ImageURL { get; set; }
    }
}
