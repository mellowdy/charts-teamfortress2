using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace chartstf
{
    public class GroupedPlayerStatline
    {
        public string Alias { get; set; }
        //public int? GamesPlayed { get; set; }
        

        public List<string> Classes { get; set; }
        public int Kills { get; set; }
        public double KillsPerMinute { get; set; }
        public double KillsAssistsPerMinute { get; set; }
        public double KillsPerDeaths { get; set; }
        public double KillsAssistsPerDeaths { get; set; }
        public int Deaths { get; set; }
        public double DeathsPerMinute { get; set; }
        public int Damage { get; set; }
        public double DamagePerMinute { get; set; }
        public int Assists { get; set; }
        public double AssistsPerMinute { get; set; }
        public int? Healing { get; set; }
        public double? HealingPerMinute { get; set; }
        public int? Ubers { get; set; }
        public double? UbersPerMinute { get; set; }
        public int? UbersDropped { get; set; }
        public List<Medigun> UberTypes { get; set; }
        //public int? Medigun { get; set; }
        //public int? Kritzkrieg { get; set; }
        //public int? Vaccinator { get; set; }
        //public int? QuickFix { get; set; }
        public double? AvgBuildTime { get; set; }
        public double? AvgTimeBeforeUsing { get; set; }
        //public int? NearFullChargeDeaths { get; set; }
        public int? AdvantagesLost { get; set; }
        public int? BiggestAdvantageLost { get; set; }
        public int? DeathsCloseToUber { get; set; }
        public double? AvgUberLength { get; set; }
        public double? AvgTimeBeforeHealing { get; set; }
        public int? DeathsAfterCharge { get; set; }
        public int? Caps { get; set; }
        public int? Suicides { get; set; }
        public int? HealthPacks { get; set; }
        public int? Backstabs { get; set; }
        public double? BackstabsPerMinute { get; set; }
        public int? Headshots { get; set; }
        public double? HeadshotsPerMinute { get; set; }
        public int? Airshots { get; set; }
        public double? AirshotsPerMinute { get; set; }
        public int? MedicPicks { get; set; }
        public double? MedicPicksPerMinute { get; set; }
        public int DamageTaken { get; set; }
        public double DamageTakenPerMinute { get; set; }
        public int? HealsReceived { get; set; }
        public double? HealPercentageReceived { get; set; }
        public TimeSpan Time { get; set; }

        // Kills that were gotten on respective classes
        public int? ScoutKills { get; set; }
        public int? SoldierKills { get; set; }
        public int? PyroKills { get; set; }
        public int? DemomanKills { get; set; }
        public int? HeavyKills { get; set; }
        public int? EngineerKills { get; set; }
        public int? MedicKills { get; set; }
        public int? SniperKills { get; set; }
        public int? SpyKills { get; set; }

        // Deaths to respective classes
        public int? ScoutDeaths { get; set; }
        public int? SoldierDeaths { get; set; }
        public int? PyroDeaths { get; set; }
        public int? DemomanDeaths { get; set; }
        public int? HeavyDeaths { get; set; }
        public int? EngineerDeaths { get; set; }
        public int? MedicDeaths { get; set; }
        public int? SniperDeaths { get; set; }
        public int? SpyDeaths { get; set; }

        // Buildings
        public int? SentriesBuilt { get; set; }
        public int? DispensersBuilt { get; set; }
        public int? TeleportersBuilt { get; set; }
        public int? SentriesDestroyed { get; set; }
        public int? DispensersDestroyed { get; set; }
        public int? TeleportersDestroyed { get; set; }

        public double? SentriesBuiltPerMinute { get; set; }
        public double? SentriesDestroyedPerMinute { get; set; }
        public double? DispensersBuiltPerMinute { get; set; }
        public double? DispensersDestroyedPerMinute { get; set; }
        public double? TeleportersBuiltPerMinute { get; set; }
        public double? TeleportersDestroyedPerMinute { get; set; }

        // Other
        public int? Extinguishes { get; set; }
        public double? ExtinguishesPerMinute { get; set; }
    }
}
