using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace chartstf
{
    public class CollectionMappingProfile : Profile
    {
        public CollectionMappingProfile()
        {
            CreateMap<Collection, CollectionModel>()
            .ForMember(dest => dest.Matches, opt => opt.MapFrom(dest => dest.CollectionsMatches.Select(p => p.Match.LogID)))
            .ReverseMap();

            CreateMap<Collection, CollectionModelForPost>()
            .ForMember(dest => dest.Matches, opt => opt.MapFrom(dest => dest.CollectionsMatches.Select(p => p.Match.LogID)))
            .ReverseMap();
        }
    }
}
