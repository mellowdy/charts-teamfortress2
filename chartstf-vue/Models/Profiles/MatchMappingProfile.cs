using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace chartstf
{
    public class MatchMappingProfile : Profile
    {
        public MatchMappingProfile()
        {
            CreateMap<Match, MatchModel>()
            .ForMember(dest => dest.Collections, opt => opt.MapFrom(dest => dest.CollectionsMatches.Select(p => p.Collection.Name)))
            .ReverseMap();

            CreateMap<CollectionsMatches, CollectionsMatchesModel>()
                .ForMember(dest => dest.LogID, opt => opt.MapFrom(p => p.Match.LogID))
                .ForMember(dest => dest.CollectionName, opt => opt.MapFrom(p => p.Collection.Name))
                .ReverseMap();

            CreateMap<MatchModel, Match>()
                .ForMember(dest => dest.ID, opt => opt.Ignore())
                .ForMember(dest => dest.CollectionsMatches, opt => opt.MapFrom(src => src.Collections
                                                        .Select(pn => new CollectionsMatchesModel(pn))));
        }
    }
}
