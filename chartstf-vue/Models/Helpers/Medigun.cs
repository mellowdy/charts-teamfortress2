using System;
using System.Collections.Generic;
using System.Text;

namespace chartstf
{
    public class Medigun
    {
        public string Name { get; set; }
        public int? Ubers { get; set; }
    }
}
