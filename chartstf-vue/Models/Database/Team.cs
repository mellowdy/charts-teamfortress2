using System;
using System.Collections.Generic;
using System.Text;

namespace chartstf
{
    public class Team
    {
        // Identifier
        public Guid ID { get; set; }
        // ETF2L ID
        public int ETF2LID { get; set; }
    }
}
