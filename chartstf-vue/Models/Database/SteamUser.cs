using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace chartstf
{
    public class SteamUser : IdentityUser
    {
        // Store SteamID
        [Key]
        [Required]
        public ulong SteamId { get; set; }
        public string Steam3ID { get; set; }

        // Names
        public string PersonaName { get; set; }

        // Profile pictures
        public string AvatarFull { get; set; }
        public string AvatarMedium { get; set; }
        public string AvatarSmall { get; set; }

        // URLs
        public string ETF2LUrl { get; set; }

        [ScaffoldColumn(false)]
        [StringLength(1024)]
        public string ProfileUrl
        {
            get
            {
                return "http://steamcommunity.com/profiles/" + this.SteamId;
            }
        }

        [ScaffoldColumn(false)]
        [StringLength(1024)]
        public string UGCUrl
        {
            get
            {
                return "https://www.ugcleague.com/players_page.cfm?player_id=" + this.SteamId;
            }
        }

        [ScaffoldColumn(false)]
        [StringLength(1024)]
        public string LogsTFUrl
        {
            get
            {
                return "http://logs.tf/profile/" + this.SteamId;
            }
        }

        [ScaffoldColumn(false)]
        [StringLength(1024)]
        public string TF2CenterUrl
        {
            get
            {
                return "http://tf2center.com/profile/" + this.SteamId;
            }
        }

        [ScaffoldColumn(false)]
        [StringLength(1024)]
        public string OZFortressUrl
        {
            get
            {
                return "https://warzone.ozfortress.com/users/steam_id/" + this.SteamId;
            }
        }

        [ScaffoldColumn(false)]
        [StringLength(1024)]
        public string RGLUrl
        {
            get
            {
                return "http://rgl.gg/Public/PlayerProfile.aspx?p=" + this.SteamId;
            }
        }

        [ScaffoldColumn(false)]
        [StringLength(1024)]
        public string BackpackTFUrl
        {
            get
            {
                return "https://backpack.tf/u/" + this.SteamId;
            }
        }
        
        [ScaffoldColumn(false)]
        [StringLength(1024)]
        public string SizzlingStatsUrl
        {
            get
            {
                return "http://sizzlingstats.com/player/" + this.SteamId;
            }
        }

        [ScaffoldColumn(false)]
        [StringLength(1024)]
        public string DemosTFUrl
        {
            get
            {
                return "https://demos.tf/profiles/" + this.SteamId;
            }
        }

        [ScaffoldColumn(false)]
        [StringLength(1024)]
        public string HLPugsUrl
        {
            get
            {
                return "https://eu.hlpugs.tf/player/" + this.SteamId;
            }
        }

        public List<Collection> Collections { get; set; }
    }
}
