using System;
using System.Collections.Generic;
using System.Text;

namespace chartstf
{
    public class Player
    {
        // Player identifier
        public Guid ID { get; set; }
        // Player name
        public string Name { get; set; }
        // Steam ID (ex. 76561198072250111)
        public int SteamID64 { get; set; }
        // steam3ID (ex. [U:1:111984383])
        public string Steam3ID { get; set; }
    }
}
