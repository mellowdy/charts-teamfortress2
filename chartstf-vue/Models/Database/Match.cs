using System;
using System.Collections.Generic;
using System.Text;

namespace chartstf
{
    public class Match
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
        public string Map { get; set; }
        public string LogID { get; set; }
        public DateTime Date { get; set; }
        public TimeSpan Time { get; set; }
        public TimeSpan PlayTime { get; set; }

        private ICollection<CollectionsMatches> _collectionsMatches;
        public virtual ICollection<CollectionsMatches> CollectionsMatches
        {
            get
            {
                return _collectionsMatches ?? (_collectionsMatches = new List<CollectionsMatches>());
            }
            set
            {
                _collectionsMatches = value;
            }
        }

        public List<Statline> Statlines { get; set; }
        //private ICollection<Statline> _statlines;
        //public virtual ICollection<Statline> Statlines
        //{
        //    get
        //    {
        //        return _statlines ?? (_statlines = new List<Statline>());
        //    }
        //    set
        //    {
        //        _statlines = value;
        //    }
        //}
    }
}
