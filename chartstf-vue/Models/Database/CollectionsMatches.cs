using System;
using System.Collections.Generic;
using System.Text;

namespace chartstf
{
    public class CollectionsMatches
    {
        public Guid MatchID { get; set; }
        public Guid CollectionID { get; set; }

        public virtual Match Match { get; set; }
        public virtual Collection Collection { get; set; }
    }
}
