using System;
using System.Collections.Generic;
using System.Text;

namespace chartstf
{
    public class Collection
    {
        // Collection identifier
        public Guid ID { get; set; }
        // Name of competition or arbitrary collection
        public string Name { get; set; }
        // Short url
        public string ShortUrl { get; set; }
        // Visible to public
        public bool? IsPrivate { get; set; }

        private ICollection<CollectionsMatches> _collectionsMatches;
        public virtual ICollection<CollectionsMatches> CollectionsMatches
        {
            get
            {
                return _collectionsMatches ?? (_collectionsMatches = new List<CollectionsMatches>());
            }
            set
            {
                _collectionsMatches = value;
            }
        }

        public string UserId { get; set; }
        public ulong SteamId { get; set; }
        public SteamUser SteamUser { get; set; }
    }
}
