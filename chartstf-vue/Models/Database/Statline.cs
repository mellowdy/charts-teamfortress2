using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace chartstf
{
    public class Statline
    {
        // Statline identifier
        [Key]
        [Required]
        public Guid ID { get; set; }
        // Match ID
        public Guid MatchID { get; set; }
        // Match
        public Match Match { get; set; }
        // Player ID
        public Guid PlayerID { get; set; }
        // Log ID
        public string LogID { get; set; }
        // Steam ID (ex. 76561198072250111)
        public int SteamID64 { get; set; }
        // steam3ID (ex. [U:1:111984383])
        [Required]
        public string Steam3ID { get; set; }
        [Required]
        public string Class { get; set; }
        [Display(Name = "Player alias")]
        public string Alias { get; set; }
        // Kills
        [Required]
        [Display(Name = "Kills")]
        public int Kills { get; set; }
        // Kills per minute
        [Required]
        [Display(Name = "Kills per minute")]
        public double KillsPerMinute { get; set; }
        [Required]
        [Display(Name = "Kills per minute")]
        public double KillsAssistsPerMinute { get; set; }
        //[Required]
        [Display(Name = "Kills per deaths")]
        public double? KillsPerDeaths { get; set; }
        //[Required]
        [Display(Name = "Kills assists per deaths")]
        public double? KillsAssistsPerDeaths { get; set; }
        // Deaths
        [Required]
        [Display(Name = "Deaths")]
        public int Deaths { get; set; }
        // Deaths per minute
        [Required]
        [Display(Name = "Deaths per minute")]
        public double DeathsPerMinute { get; set; }
        // Damage
        [Required]
        [Display(Name = "Damage")]
        public int Damage { get; set; }
        // Damage per minute
        [Required]
        [Display(Name = "Damage per minute")]
        public double DamagePerMinute { get; set; }
        // Assists
        [Required]
        [Display(Name = "Assists")]
        public int Assists { get; set; }
        // Assists per minute
        [Required]
        [Display(Name = "Assists per minute")]
        public double AssistsPerMinute { get; set; }
        // Healing
        [Display(Name = "Healing")]
        public int? Healing { get; set; }
        // Healing per minute
        [Display(Name = "Healing per minute")]
        public double? HealingPerMinute { get; set; }
        // Ubers
        [Display(Name = "Ubers")]
        public int? Ubers { get; set; }
        // Ubers per minute
        [Display(Name = "Ubers per minute")]
        public double? UbersPerMinute { get; set; }
        // Ubers dropped
        [Display(Name = "Ubers dropped")]
        public int? UbersDropped { get; set; }
        // Medigun ubers
        [Display(Name = "Medigun ubers")]
        public int? Medigun { get; set; }
        // Kritzkrieg ubers
        [Display(Name = "Kritzkrieg ubers")]
        public int? Kritzkrieg { get; set; }
        // Vaccinator ubers
        [Display(Name = "Vaccinator ubers")]
        public int? Vaccinator { get; set; }
        // Quick Fix ubers
        [Display(Name = "Quick-Fix ubers")]
        public int? QuickFix { get; set; }
        // Average time to build
        [Display(Name = "Average build time")]
        public double? AvgBuildTime { get; set; }
        // Average time before using
        [Display(Name = "Average time before using")]
        public double? AvgTimeBeforeUsing { get; set; }
        // Near full charge deaths
        //[Display(Name = "Near full charge deaths")]
        //public int? NearFullChargeDeaths { get; set; }
        // Advantages lost
        [Display(Name = "Advantages lost")]
        public int? AdvantagesLost { get; set; }
        // Biggest advantage lost
        [Display(Name = "Biggest advantage lost")]
        public int? BiggestAdvantageLost { get; set; }
        // Deaths with 95-99% uber
        [Display(Name = "Deaths with 95-99% uber")]
        public int? DeathsCloseToUber { get; set; }
        // Average uber length
        [Display(Name = "Average uber length")]
        public double? AvgUberLength { get; set; }
        // Average time before healing
        [Display(Name = "Average time before healing")]
        public double? AvgTimeBeforeHealing { get; set; }
        // Deaths after charge
        [Display(Name = "Deaths after ubercharge (20s)")]
        public int? DeathsAfterCharge { get; set; }
        // Capture Point Captures
        [Display(Name = "Capture Point Captures")]
        public int? Caps { get; set; }
        // Suicides
        [Display(Name = "Suicides")]
        public int? Suicides { get; set; }
        // Health pack picked up
        public int? HealthPacks { get; set; }
        // Backstabs
        [Display(Name = "Backstabs")]
        public int? Backstabs { get; set; }
        // Backstabs per minute
        [Display(Name = "Backstabs per minute")]
        public double? BackstabsPerMinute { get; set; }
        // Headshots
        [Display(Name = "Headshots")]
        public int? Headshots { get; set; }
        // Headshots per minute
        [Display(Name = "Headshots per minute")]
        public double? HeadshotsPerMinute { get; set; }
        // Airshots
        [Display(Name = "Airshots")]
        public int? Airshots { get; set; }
        // Airshots per minute
        [Display(Name = "Airshots per minute")]
        public double? AirshotsPerMinute { get; set; }
        // Medic picks
        [Display(Name = "Medic picks")]
        public int? MedicPicks { get; set; }
        // Medic picks per minute
        [Display(Name = "Medic picks per minute")]
        public double? MedicPicksPerMinute { get; set; }
        // Damage taken
        [Display(Name = "Damage taken")]
        public int DamageTaken { get; set; }
        // Damage taken per minute
        [Display(Name = "Damage taken per minute")]
        public double DamageTakenPerMinute { get; set; }
        // Heals received
        [Display(Name = "Heals received")]
        public int? HealsReceived { get; set; }
        // Heals received
        [Display(Name = "Heal percentage received")]
        public double? HealPercentageReceived { get; set; }
        // Time played
        [Required]
        [Display(Name = "Time played")]
        public TimeSpan Time { get; set; }

        // Kills that were gotten on respective classes
        [Display(Name = "Scout kills")]
        public int? ScoutKills { get; set; }
        [Display(Name = "Soldier kills")]
        public int? SoldierKills { get; set; }
        [Display(Name = "Pyro kills")]
        public int? PyroKills { get; set; }
        [Display(Name = "Demoman kills")]
        public int? DemomanKills { get; set; }
        [Display(Name = "Heavy kills")]
        public int? HeavyKills { get; set; }
        [Display(Name = "Engineer kills")]
        public int? EngineerKills { get; set; }
        [Display(Name = "Medic kills")]
        public int? MedicKills { get; set; }
        [Display(Name = "Sniper kills")]
        public int? SniperKills { get; set; }
        [Display(Name = "Spy kills")]
        public int? SpyKills { get; set; }

        // Deaths to respective classes
        [Display(Name = "Scout deaths")]
        public int? ScoutDeaths { get; set; }
        [Display(Name = "Soldier deaths")]
        public int? SoldierDeaths { get; set; }
        [Display(Name = "Pyro deaths")]
        public int? PyroDeaths { get; set; }
        [Display(Name = "Demoman deaths")]
        public int? DemomanDeaths { get; set; }
        [Display(Name = "Heavy deaths")]
        public int? HeavyDeaths { get; set; }
        [Display(Name = "Engineer deaths")]
        public int? EngineerDeaths { get; set; }
        [Display(Name = "Medic deaths")]
        public int? MedicDeaths { get; set; }
        [Display(Name = "Sniper deaths")]
        public int? SniperDeaths { get; set; }
        [Display(Name = "Spy deaths")]
        public int? SpyDeaths { get; set; }

        // Buildings
        public int? SentriesBuilt { get; set; }
        public int? DispensersBuilt { get; set; }
        public int? TeleportersBuilt { get; set; }
        public int? SentriesDestroyed { get; set; }
        public int? DispensersDestroyed { get; set; }
        public int? TeleportersDestroyed { get; set; }

        // Other
        public int? Extinguishes { get; set; }
        public double? ExtinguishesPerMinute { get; set; }
    }
}
